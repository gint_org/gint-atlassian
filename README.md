# Description

Gint Atlassian is a Gradle plugin that extends the **Gradle Integration Test and Automation Framework** plugin by
providing enablers to make it easier to create integration tests or automation related to various Atlassian related services like Jira, Service Desk, Confluence, Bitbucket, Bamboo, Slack, and others using the Atlassian Command Line Interface (CLI).

# Links

* [Gint Repository](https://bitbucket.org/gint_org/gint)
* [Gint Atlassian Repository](https://bitbucket.org/gint_org/gint-atlassian)
* [Gint Examples Repository](https://bitbucket.org/gint_org/gint-examples)
* [Documentation Site](https://ginthome.atlassian.net/wiki/display/GINT) - extensive user documentation
* [Jira Issues](https://ginthome.atlassian.net/browse/GINT) - support and improvement requests
* [Atlassian Command Line Interface (CLI)](https://bobswift.atlassian.net/wiki/display/ACLI) (commercial)
* [Groovy Tasks for Bamboo](https://bobswift.atlassian.net/wiki/display/BGTP) - Bamboo tasks for Gint, Groovy, and Gradle (commercial)


# Developer Information
This Gradle plugin's integration tests are primarily done using itself. In order to property run all tests in a local development environment,
access is need to Jira and Confluence server instances with corresponding Atlassian CLI configurations. This is best done in a user configuration file independent of the plugin and referenced with a environment variable.

## Example configuration
```
export ACLI_CONFIG=~/acli-gint.properties

with ~/acli-gint.properties file containing something similar to:
jira       = jira       -s http://localhost:8080 -u admin -p admin
confluence = confluence -s http://localhost:8090 -u admin -p admin
```
