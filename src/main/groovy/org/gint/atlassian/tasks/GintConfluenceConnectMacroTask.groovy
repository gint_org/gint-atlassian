/*
 * Copyright (c) 2015 Appfire Technologies, Inc.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Bob Swift Atlassian Add-ons EULA"
 * (https://bobswift.atlassian.net/wiki/x/WoDXBQ) as well as under the provisions of
 * the "Standard EULA" from the "Atlassian Marketplace Terms of Use" as a "Marketplace Product”
 * (http://www.atlassian.com/licensing/marketplace/termsofuse).
 *
 * See the LICENSE file for more details.
 */

package org.gint.atlassian.tasks

import org.gint.atlassian.helpers.SeleniumHelperForConfluence
import org.gint.plugin.Constants
import org.gint.plugin.GintUtils

import groovy.transform.Synchronized

public class GintConfluenceConnectMacroTask extends GintConfluenceMacroTask {

    /**
     * Constructor
     */
    GintConfluenceConnectMacroTask() {
        super()
    }

    /**
     * Set outData for the task to data from appropriate places. Override in sub class if necessary.
     * @param task
     */
    @Override
    protected void resetOutData() {

        Map map = hasProperty(Constants.FIELD_NAME_USER_MAP) ? this[Constants.FIELD_NAME_USER_MAP]: [:]
        //helper.log 'reset outData map', map

        if (gint.seleniumHelper instanceof SeleniumHelperForConfluence) {
            getConnectRenderedData(map + [throwException: true])
        } else {
            super.resetOutData()
        }
    }

    /**
     * Connect rendered data - we need to wait for the iframe and then go there. The iframe is added to the task map.
     * This assumes all the pre-conditions in resetOutData!!!
     * @param task user map
     */
    @Synchronized
    protected void getConnectRenderedData(final Map map) {

        assert map != null // getConnectRenderedData must have parameters

        SeleniumHelperForConfluence seleniumHelper = (SeleniumHelperForConfluence) gint.seleniumHelper

        outData = [] // default to empty
        if (map.connectHtmlFile == null) {
            map.connectHtmlFile = gint.getOutputFile(name, '-connect.html')
        }
        map.renderedMacroFile = gint.getOutputFile(map.name, '.txt')
        GintUtils.deleteFileQuietly(map.renderedMacroFile)
        //helper.log 'connect reset outData map', map
        //helper.log 'result', result

        if (result == 0) {
            try {
                seleniumHelper.gotoPage([
                    space: map.space,
                    title: map.title ?: this.name,
                    throwException: map.throwException,
                ])
                def findVerbose = false

                def element = seleniumHelper.switchToConnectMacroFrame(map)

                // It is important to remember the frame in the task's map for later use in a success closure for example
                this.map.frame = element

                if (element != null) {
                    element = seleniumHelper.findElement(id: gint.getConfluenceHelper().getConnectContentId(), wait: map.wait, delay: map.delay, visible: true, screenshot: map.screenshot != null ? map.screenshot : true, verbose: findVerbose)
                    if (element == null) {
                        message 'error', 'Not found element: ' + gint.getConfluenceHelper().getConnectContentId() + '. Add-on data missing standard div. No rendered data available.'
                    } else {
                        outData = [
                            element.getAttribute('outerHTML')
                        ]
                        // Ensure the connect data gets written to a file for easier debugging, similar to the non-connect txt file
                        new File(map.connectHtmlFile).write(outData)
                    }
                }
            } catch (Exception | Error exception) {  // primarily for assertion failures - other wise task was blowing up trying to end
                def error = "Error getting connect rendered data for task ${this.name}. Error: " + exception.toString()
                if (map.throwException == true) {
                    gint.printStackTrace(exception) // only if option set
                    throw new Exception(error)
                }
                gint.addFailedMessage(error, false) // false to not cause test error

            } finally {
                // Switch back to main document
                seleniumHelper.getDriver().switchTo().defaultContent()
            }
        }
    }
}
