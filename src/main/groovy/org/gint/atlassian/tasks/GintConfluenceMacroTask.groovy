/*
 * Copyright (c) 2015 Appfire Technologies, Inc.
 * All rights reserved.
 *
 * This software is licensed under the provisions of the "Bob Swift Atlassian Add-ons EULA"
 * (https://bobswift.atlassian.net/wiki/x/WoDXBQ) as well as under the provisions of
 * the "Standard EULA" from the "Atlassian Marketplace Terms of Use" as a "Marketplace Product”
 * (http://www.atlassian.com/licensing/marketplace/termsofuse).
 *
 * See the LICENSE file for more details.
 */

package org.gint.atlassian.tasks

import org.gint.plugin.Constants
import org.gint.tasks.GintTask

public class GintConfluenceMacroTask extends GintTask {

    /**
     * Constructor
     */
    GintConfluenceMacroTask() {
        super()
    }

    /**
     * Set outData for data compare. Override in sub class if necessary.
     * @param task
     */
    @Override
    protected void resetOutData() {

        Map map = hasProperty(Constants.FIELD_NAME_USER_MAP) ? this[Constants.FIELD_NAME_USER_MAP]: null
        //helper.log 'reset outData map', map
        //helper.log 'task', this

        if (map?.renderedMacroFile != null) {
            outData = []
            try {
                def file = new File(map.renderedMacroFile)  // should always exist except if store failed
                if (file.exists()) {
                    outData = readFileToList(file)
                } else { // store failed, then lets get the error information. Like macro migration errors
                    super.resetOutData()
                }
            } catch (IOException exception) {
                if (helper.isDebug()) {
                    helper.log('task map', map)
                }
                throw new Exception('Unable to read attachment file. ' + exception.toString())
            }
        } else {
            super.resetOutData()  // likely a CLI error, so lets get the normal output from that
        }
    }
}
