/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.helpers.ParallelHelper
import org.gint.interfaces.CmdGenerator
import org.gint.plugin.Gint
import org.gint.tasks.GintTask

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Gint extensions for Confluence
 */
@CompileStatic
@TypeChecked
class ConfluenceHelper extends AtlassianHelper {

    boolean isConnect = false  // default for macro and render tasks to use connect processing

    protected String parent = null  // default parent for Confluence pages that are created
    protected String spaceKey

    protected String connectContentId = 'webcontent-all' // TOOD - temporary until all test

    protected final PARENT = '@home'  // default parent location for confluence pages
    protected final GINT_ATTACHMENT = 'gint.html'
    protected final DEFAULT_MACRO_ERROR_INDICATOR = [
        // '<div class="error', // this is the older style error
        ~/(?:(?:<div class="error)|(?:<div class="[^"]*? aui-message-error))/, // covers both older style and newer (like Confleucne 6.5) style errors
    ]

    /**
     * Constructor
     */
    ConfluenceHelper(final Gint gint) {
        super(gint, 'confluence')
        //setShowFailDetail(false) // reproduce command not useful in this case
    }

    /**
     * Get a generated space name for use in testing Confluence apps
     * @return a space name
     */
    public String getSpaceName() {
        return gint.getConstructName()
    }

    /**
     * Set space name
     * @param space name
     */
    public void setSpaceName(final String name) {
        gint.setConstructName(name)
    }

    /**
     * Get a generated space key for use in testing Confluence apps
     * @return a space name
     */
    public String getSpaceKey() {
        if (spaceKey == null) {  // provide a default key
            spaceKey = getSpaceName().replace(' ', '')
        }
        return spaceKey
    }

    /**
     * Set space key
     * @param space key
     */
    public void setSpaceKey(final key) {
        this.spaceKey = key
    }

    protected String getConnectContentId() {
        return connectContentId
    }

    protected void setConnectContentId(final String id) {
        connectContentId = id
    }

    /**
     * Set default parent for Confluence pages
     * @param parent
     */
    public void setParent(final parent) {
        this.parent = parent.toString()
    }

    /**
     * Get current default parent
     */
    public String getParent() {
        return (this.parent ?: PARENT)
    }

    public String getCmdGeneratorName() {
        return 'confluenceMacro'  // usually defaulted to confluenceMacro
    }

    /**
     * Override this so default gets set automatically in initialize after properties have been read
     * @return current command generator
     */
    //    @Override
    //    public Closure getCmdGenerator() {
    //        return this.defaultCmdGenerator ?: getGintCmdGenerator().getGenerator('confluenceMacro', getCmdGeneratorParameters())  //  only change setting if not already set
    //    }

    public Map<String, Object> getCmdGeneratorParameters() {
        def errorIndicator = helper.getParameterValue('macroErrorIndicator')
        def regex = helper.getParameterValue('macroErrorIndicatorRegex') // use this instead of macroErrorIndicator if provided
        if (regex != null) {
            errorIndicator = java.util.regex.Pattern.compile(regex)
        } else if (errorIndicator == null) {
            errorIndicator = DEFAULT_MACRO_ERROR_INDICATOR // default if nothing provided
        }

        return [
            macroPageBegin: helper.getParameterValue('macroPageBegin', ''),
            macroPageEnd:   helper.getParameterValue('macroPageEnd', ''),
            macroStoragePageBegin: helper.getParameterValue('macroStoragePageBegin', ''),
            macroStoragePageEnd:   helper.getParameterValue('macroStoragePageEnd', ''),
            macroErrorIndicator: helper.isNotBlank(errorIndicator) ? [errorIndicator]: null,
            wiki: helper.getBooleanParameterValue('wiki', false),
            //attachment:  GINT_ATTACHMENT,
            cli: helper.getParameterValue('cli'), // makes it easy to change cli from command line, confluenceCli could also be used
        ]
    }

    /**
     * Easier for Confluence tasks to define action or macro or render and a name extension - this avoids a lot of duplication in task definition
     * Example:  action: 'info', ext: 'NoIcon' -> results in name: 'infoNoIcon'
     * @return closure that takes a task and sets its name field (if not defined)
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Closure getAddTaskClosure() {
        return { Map map ->
            if (!helper.isNotBlank(map.name)) {
                map.name = (map.action ?: (map.macro ?: (map.render ?: ''))) + (map.ext != null ? map.ext : '')
            }

            // TODO: this needs special support
            //            if (map.render != null) {
            //                GintTask referencedTask = findTask(map.render, '', false)
            //                if (referencedTask != null) {
            //                    map.dependsOn = helper.getAsCollection(map.dependsOn ?: map.depends) + [
            //                        referencedTask.name ] // and referenced map to dependencies
            //                }
            //            }
        }
    }

    /**
     * Set outData for the task to data from appropriate places. Override in sub class if necessary.
     * @param task
     */
    // TODO:
    //@Override
    //    protected void setOutData(final GintTask task) {
    //        if ((task.map?.macro == null) && (task.map?.render == null)) {
    //            super.setOutData(task)
    //        } else if (isConnecttask(task) && !task.isTearDown) {
    //            getConnectRenderedData(task)
    //        } else if (task.renderedMacroFile != null) {
    //            task.outData = []
    //            try {
    //                def file = new File(task.renderedMacroFile)  // should always exist except if store failed
    //                if (file.exists()) {
    //                    task.outData = readFileToList(file, task)
    //                } else { // store failed, then lets get the error information. V4 this is likely to occur for macro migration errors
    //                    super.setOutData(task)
    //                }
    //            } catch (IOException exception) {
    //                if (gint.verbose) {
    //                    exception.printStackTrace()
    //                }
    //                if (helper.isDebug()) {
    //                    helper.log('task', task)
    //                }
    //                throw new Exception('Unable to read attachment file. ' + exception.toString())
    //            }
    //        }
    //    }

    /**
     * Connect rendered data - we need to wait for the iframe and then go there
     * @param task
     */
    // TODO:
    //    @Synchronized
    //    protected void getConnectRenderedData(GintTask task) {
    //        def seleniumHelper = getSeleniumHelper(task) // assumed to be non null before calling
    //        task.outData = [] // default to empty
    //        if (task.connectHtmlFile == null) {
    //            task.connectHtmlFile = getOutputFile(task.name, '-connect.html')
    //        }
    //        binding.ant.delete(file: task.renderedMacroFile, quiet: true) // make sure there is no old data
    //
    //        try {
    //            if (task.result == 0) {
    //                seleniumHelper.gotoPage(space: task.space, title: task.name)
    //
    //                def element
    //                def findVerbose = false
    //
    //                element = seleniumHelper.switchToConnectMacroFrame(task)
    //                task.frame = element
    //                if (element != null) {
    //                    element = seleniumHelper.findElement(id: getConnectContentId(), wait: task.wait, delay: task.delay, visible: true, screenshot: task.screenshot != null ? task.screenshot : true, verbose: findVerbose)
    //                    if (element == null) {
    //                        message 'error', 'Not found element: ' + getConnectContentId() + '. Add-on data missing standard div. No rendered data available.'
    //                    } else {
    //                        task.outData = [
    //                            element.getAttribute('outerHTML')
    //                        ]
    //                        // Ensure the connect data get written to a file for easier debugging, similar to the non-connect txt file
    //                        new File(task.connectHtmlFile).write(task.outData)
    //                    }
    //                    // Set back to main document
    //                    seleniumHelper.getDriver().switchTo().defaultContent()
    //                }
    //            }
    //        } catch (Error error) {  // primarily for assertion failures - other wise task was blowing up trying to end
    //            addTestFailedMessage("Error getting connect rendered data for task ${task.name}. Error: " + error.toString(), false) // false to not cause test error
    //            handleError(task, error)
    //        }
    //    }

    /**
     * task has ended and is about to report outcome. This is an opportunity to decide to log additional information.
     * Default is to log output from parallel tasks if verbose is set
     * @param task - task that has ended
     */
    // TODO:
    //    @Override
    //    protected void logOutputAfterEnd(final Map task, final boolean ignoreFailure = false) {
    //        if (getVerbose() || !(task.success || ignoreFailure)) {
    //            if (task.parallel) {
    //                if ((true != task?.action?.toLowerCase()?.startsWith('render')) || helper.isDebug() || (task.parallel.getOutList().size() < 50)) {
    //                    task.parallel.printOutList()
    //                } else { // limit huge displays of render pages in logs unless requested
    //                    message 'info', 'GINT: Render output data suppressed due to size. Use debug to list.'
    //                }
    //                task.parallel.printErrorList()
    //            }
    //        }
    //    }

    /**
     * Add standard Confluence space management tasks
     */
    public List<GintTask> addSpaceManagementTasks(final Map map = null) {

        def postFix = map?.postFix ?: map?.postfix ?: ''
        Map localMap = (Map) (map instanceof Map ? new HashMap(map) : [:])
        localMap.name = localMap.name ?: 'addSpace' + (localMap?.space ?: postFix)
        boolean clearSpaceContent = localMap.containsKey('clearSpaceContent') ? localMap.clearSpaceContent : helper.getBooleanParameterValue('clearSpaceContent')

        List<GintTask> list = []
        if (clearSpaceContent) {
            if (!(localMap.parameters instanceof Map)) {
                localMap.parameters = new HashMap()
            }
            ((Map) localMap.parameters).options = 'clearContent' // CLI option is clearContent
            localMap.retry = localMap.retry ?: 0 // no retries when not removing space unless explicitly requested
            list.add(addAddSpaceTask(localMap))
        } else {
            String tearDownName = ((String)localMap.name).replace('addSpace', 'removeSpace')
            if (tearDownName == localMap.name) {
                tearDownName = 'removeSpace' + (localMap?.space ?: postFix)
            }
            list.add(addAddSpaceTask(localMap + [tearDown: tearDownName]))
            list.add(addRemoveSpaceTask(localMap + [name: tearDownName]))
        }
        return list
    }

    /**
     * Add standard Confluence space
     * @return
     */
    public GintTask addAddSpaceTask(final Map map = null) {
        return (GintTask) gint.taskHelper.addSetUp(getAddSpaceTaskMap(map))
    }

    /**
     * Add standard Confluence space
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected Map<String, Object> getAddSpaceTaskMap(final Map map = null) {

        def postFix = map?.postFix ?: map?.postfix ?: ''
        String name = map?.name ?: 'addSpace' + (map?.space ?: postFix)
        def retry = map?.containsKey('retry') ? map.retry : 3
        return [
            action: 'addSpace', name: name,
            level: map?.level,
            onlyIf: map?.onlyIf,
            depends: map?.depends,
            space: map?.space ?: getSpaceKey() + postFix,  // need a space key without blanks
            parameters: [
                name: map?.space ?: getSpaceName() + postFix,
                continue: null, // ok if it already exists
            ] + (map?.parameters ?: [:]),
            finalClosure: map?.finalClosure,
            // tearDown: map?.containsKey('tearDown') ? map.tearDown : 'removeSpace' + postFix, // don't override user choice
            retry: retry, // apparently, Confluence may now be deleting spaces in the background. Getting errors on slower systems.
            retrySleep: 3000,
        ] + (map?.cmdGenerator ? [cmdGenerator: map?.cmdGenerator] : [:])
    }

    public GintTask addRemoveSpaceTask(final Map map = null) {
        return (GintTask) gint.taskHelper.addTearDown(addRemoveSpaceTaskMap(map))
    }

    /**
     * Remove standard Confluence space
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected Map<String, Object> addRemoveSpaceTaskMap(final Map map = null) {
        def postFix = map?.postFix ?: map?.postfix ?: ''
        String name = map?.name ?: 'removeSpace' + (map?.space ?: postFix)
        def retry = map?.containsKey('retry') ? map.retry : 3
        return [
            action: 'removeSpace', name: name,
            level: map?.level,
            dependsOn: map?.dependsOn,
            space: map?.space ?: getSpaceKey() + postFix,  // need a space key without blanks
            parameters: [
                continue: null, // ok if it does not exists
            ] + (map?.parameters ?: [:]),
            retry: retry,
            // Retry only if we get this error - this has been noticed occasionally, second remove attempt seems to work
            // This has started occurring on Confluence 4.x
            retryData: 'LazyInitializationException',
        ] + (map?.cmdGenerator ? [cmdGenerator: map?.cmdGenerator] : [:])
    }

    public GintTask addAddSpacePermissionsTask(final Map map = null) {
        return (GintTask) gint.taskHelper.addSetUp(getAddSpacePermissionsTaskMap(map))
    }

    // administrators for Cloud, confluence-administrators for server\\\
    /**
     * Handle granting all space permissions to administrators.
     * @param group - a single group that defaults to administrators (Cloud uses this)
     * @param info  - standard info map that contains isCloud boolean to choose which group to add
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected Map<String, Object> getAddSpacePermissionsTaskMap(final Map parameters) {
        def postFix = parameters?.postFix ?: ''
        def group = parameters?.info == null || parameters?.info?.isCloud == true || parameters?.isCloud == true ? 'administrators' : 'confluence-administrators'
        return [
            action: 'addPermissions', name: 'addSpacePermissions' + postFix, description: 'So administrators have access for sure',
            depends: ['addSpace' + postFix],
            space: getSpaceKey() + postFix,
            parameters: [
                permissions: '@all',
                group: group,
            ]
        ]
    }

    public GintTask addCheckAppTask(final Map map = null) {
        return (GintTask) gint.taskHelper.add(getAppTaskMap(map))
    }

    /**
     * Get a check addon task - verifies addon is installed and enabled
     * @param key - plugin key, example: org.swift.confluence.table
     * @param name - name of task to be generated, defaults to checkApp_<key>
     * @return
     */
    protected Map<String, Object> getAppTaskMap(final Map parameters) {
        assert parameters?.key != null
        return [
            action: 'getApp', name: (parameters.name ?: "checkApp_${parameters.key}"),
            parameters: [
                app: parameters?.key,
            ],
            ignoreFailure: parameters?.ignoreFailure,
            data: [
                'Enabled . . . . . . . . . . . : Yes',
            ],
        ]
    }

    /**
     * Check for server configuration and availability.
     * @param cli - cli to use, otherwise it will default to standard confluence cli lookup
     * @return true if cli is configured and server is available and set parameters.info to the server info
     */
    // TODO: super class Atlassian seems to cover this, check to remove for other clients
    //    @Override
    //    public boolean isServerAvailable(final Map parameters = null) {
    //        def cli = getCli(parameters?.cli)
    //        def info = getServerInfo(cli)  // use to verify cli is configured and server is available
    //        boolean result = (info != null)
    //        if (parameters != null) {
    //            parameters.info = info
    //        }
    //        return result
    //    }

    /**
     * Contact server using the configured cli and retrieve information.
     * Be as generic as possible on output scanning to handle CLI changes - ignore case and spacing.
     * <pre>
     * Expected output: "Confluence version: 3.1.0, build: 1722, xxx, url: http://localhost:8107"
     * </pre>
     * @return Map with version, build, and url setting. Null if confluence cannot be contacted.
     */
    public Map<String, Object> getServerInfo(final inCli = null) {
        return getServerInfo(inCli instanceof Map ? inCli : [cli: inCli])
    }

    @Override
    public Map<String, Object> getServerInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {  // not explicitly configured
            def cmd = cli.trim() + ' --action getServerInfo --outputFormat 2 --dateFormat "' + helper.getJsonDateFormatString() + '"'
            def p = gint.parallelHelper.shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()
            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]
                info.cli = cli
                int index = 0

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing
                //System.out.println('xxx: ' + string)
                info.product           = helper.getPrettyFormatValue(string, 'product')
                info.hosting           = helper.getPrettyFormatValue(string, 'hosting')
                info.version           = helper.getPrettyFormatValue(string, 'version')
                info.productIsLicensed = helper.getPrettyFormatValue(string, 'product is licensed')
                info.url               = helper.getPrettyFormatValue(string, 'base url') // different than others
                info.title             = helper.getPrettyFormatValue(string, 'title')
                info.build             = helper.getPrettyFormatValue(string, 'build')
                info.buildDate         = helper.getPrettyFormatValue(string, 'build date')
                info.time              = helper.getPrettyFormatValue(string, 'current time')
                info.timeZone          = helper.getPrettyFormatValue(string, 'time zone')
                info.readOnly          = helper.getPrettyFormatValue(string, 'read only')
                info.connectorVersion  = helper.getPrettyFormatValue(string, 'connector version')
                info.license           = helper.getPrettyFormatValue(string, 'license information')
                info.client            = helper.getPrettyFormatValue(string, 'client')
                info.user              = helper.getPrettyFormatValue(string, 'user')
                info.userKey           = helper.getPrettyFormatValue(string, 'user key')          // CLI 9.6
                info.userDisplayName   = helper.getPrettyFormatValue(string, 'user display name') // CLI 9.6

                info.isReadOnly        = info.readOnly == 'Yes' // boolean value is better for tests

                // Needs CLI 9.2 or higher for hosting
                info.isCloud           = info.hosting == 'cloud' || ((String)info.client)?.contains('cloud') || helper.compareSeparatedGreaterThanOrEqual(info.version?.toString(), '1000')
                info.isServer          = info.hosting == 'server'
                info.isDatacenter      = info.hosting == 'datacenter'

                if (info.url != null) {
                    def matcher2 = info.url =~ /\S*\/\/[^\/]*(\S*)/
                    info.contextPath = (matcher2.matches()) ? matcher2.group(1) ?: "" : ""
                }

                helper.removeNullValues(info)

                if (info.product == null) {
                    p.printOutList()
                    addMessage 'warning', 'Could not determine server information from the data returned from the request.'
                }

            } else if (gint.verbose) {
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', "Configuration missing: cli and confluenceCli properties are blank or not defined."
        }
        if (gint.verbose) {
            if (info) {
                helper.log('serverInfo', info)
                helper.log('spaceKey', getSpaceKey())  // show default space key
                helper.log('spaceName', getSpaceName()) // show default space name
            } else {
                helper.log('cli', cli)
            }
        }
        return info
    }

    /**
     * Return the cli that will be used
     * @return
     */
    public String getCli(final inCli = null) {
        return helper.getWithExtendedLookup(inCli ?: helper.getParameterValue('cli'), 'confluenceCli', 'acli confluence')
    }

     public Map<String, Object> getUserInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {
            def cmd = cli.trim() + ' --action getUser ' + (helper.isNotBlank(parameters.userId) ? '--userId ' + parameters.userId : '')
            def p = gint.getParallelHelper().shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()
            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing

                info.user              = helper.getPrettyFormatValue(string, 'user')  // CLI 10.2 - Confluence is user not user id
                info.userKey           = helper.getPrettyFormatValue(string, 'user key')
                info.userDisplayName   = helper.getPrettyFormatValue(string, 'full name')
                info.userEmail         = helper.getPrettyFormatValue(string, 'email')

            } else if (gint.getVerbose()) {
                //message 'debug', "rc: ${rc}"
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', "Configuration missing: cli and confluenceCli properties are blank or not defined."
        }
        return info
    }

    /**
     * Get the page url for the page identified by the parameters. Accesses the server for data. Example space: xxx, title: yyyy. Another example: id: ddddd
     * @param parameters
     * @return map of page info or null if there was an error
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Map<String, Object> getPageInfo(final Map parameters) {

        def info = [:] // initialize returned data

        assert parameters != null // getPageInfo needs parameters defined

        Map map = parameters + [
            action: 'getPage',
        ]
        // helper.log 'getPageInfo map', map

        def cmd = gint.cmdGeneratorHelper.getCmdForConfluence(gint.cmdGeneratorHelper.getCli('confluence', parameters), map)
        //helper.log 'getPageInfo cmd', cmd

        ParallelHelper p = (ParallelHelper) helper.runCmd(cmd: cmd, returnParallel: true, verbose: map.verbose == true)
        if (p.getReturnCode() == 0) {
            def data = helper.listToSeparatedString(list: p.getOutList(), separator: '\n')

            info.id            = helper.getPrettyFormatValue(data, 'Page id')
            info.title         = helper.getPrettyFormatValue(data, 'Title')
            info.space         = helper.getPrettyFormatValue(data, 'Space key')
            info.spaceName     = helper.getPrettyFormatValue(data, 'Space name')
            info.spaceHomePage = helper.getPrettyFormatValue(data, 'Space home page title')
            info.version       = helper.getPrettyFormatValue(data, 'Version')
            info.url           = helper.getPrettyFormatValue(data, 'URL')
            info.creator       = helper.getPrettyFormatValue(data, 'Creator')
            info.created       = helper.getPrettyFormatValue(data, 'Created')
            info.modifier      = helper.getPrettyFormatValue(data, 'Modifier')
            info.modified      = helper.getPrettyFormatValue(data, 'Modified')
            info.isHome        = helper.getPrettyFormatValue(data, 'Is home page') == 'Yes' // boolean value
            info.status        = helper.getSingleFind(data, ~/Status {0,1}(?: .)* : (\S*)/)

            helper.removeNullValues(info)

            info.isCloud = info.url.contains(".atlassian.net/") // boolean value

            if (info.isCloud) {
                info.baseUrl =  info.url.replaceFirst('(.*?/wiki).*', '$1')
                info.urlViewByIdRelative = "/spaces/${info.space}/pages/${info.id}"
            } else {
                Integer index = info.url?.toString()?.indexOf('/display')
                if (index < 0) {
                    index = info.url?.toString()?.indexOf('/pages')
                }
                info.baseUrl = index > 0 ? info.url?.toString()?.substring(0, index) : ""
                info.urlViewByIdRelative = "/pages/viewpage.action?pageId=${info.id}"
            }
            info.urlViewById = info.baseUrl.toString() + info.urlViewByIdRelative
        } else {
            info = null // callers responsibility to report error
        }
        if (gint.verbose || (map.verbose == true)) {
            helper.log('page info', info)
        }
        return info
    }

    /**
     * Convert wiki to storage format. Current cmd generator will be used if something different not specified
     * @param parameters - any of the standard task content parameters like content, content2, file.
     * @return storage format from running command
     */
    //@CompileStatic(TypeCheckingMode.SKIP)
    public String convertToStorageFormat(final GintTask task, final Map parameters) {
        assert parameters != null // convertToStorageFormat needs parameters

        def result = null // initialize result

        Map map = gint.getCmdGeneratorHelper().getUserMap(task)   // need space and title or id
        CmdGenerator cmdGenerator = getCmdGeneratorFromMap(map)
        assert cmdGenerator != null // script or task parameters must define a cmd generator to use

        map.cmdGeneratorParameterAugments = parameters + [ // space or title are NOT needed in this case
            action: 'convertToStorageFormat',
        ]

        def cmd = cmdGenerator.callback(task)

        ParallelHelper p = (ParallelHelper) helper.runCmd(cmd: cmd, returnParallel: true, verbose: parameters.verbose == true)
        if (p.getReturnCode() == 0) {
            result = helper.listToSeparatedString(list: p.getOutList(), separator: '\n')
        }
        if (gint.verbose || (parameters.verbose == true)) {
            helper.log('storage format', result)
        }
        return result
    }

    /**
     * Get a valid cmd generate from a map defaulting to global cmd generator if needed
     * @param map
     * @return cmd generator or null
     */
    public CmdGenerator getCmdGeneratorFromMap(final Map map) {
        return map.cmdGenerator instanceof CmdGenerator ? (CmdGenerator) map.cmdGenerator : gint.getCmdGenerator()
    }
}
