/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.helpers.BaseHelper
import org.gint.plugin.Gint
import org.gint.tasks.GintTask

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * GINT extensions for JSAP based CLI products.
 * Provides functions to support CLI products based on JSAP - Java Simple Argument Parser
 */
@CompileStatic
@TypeChecked
class JsapHelper extends BaseHelper {

    protected String name // default cmd generator name, client name

    /**
     * Constructor
     */
    JsapHelper(final Gint gint, final String name = null) {
        super(gint)
        this.name = name ?: 'jsap'
        // only change setting if not already set
        if (gint.cmdGenerator == null) {
            setCmdGenerator()
        }
        if (gint.addTaskClosure == null) {
            gint.addTaskClosure = getAddTaskClosure()
        }
        if (gint.cmdLogClosure == null) {
            gint.cmdLogClosure = getCmdLogClosure(name)
        }
    }

    public setCmdGenerator() {
        gint.setCmdGenerator(getCmdGeneratorName(), getCmdGeneratorParameters())
    }

    public String getCmdGeneratorName() {
        return name
    }

    public Map<String, Object> getCmdGeneratorParameters() {
        return [cli: (Object) helper.getParameterValue('cli')] // makes it easy to change cli from command line
    }

    /**
     * Get a closure that is used to cleanup output for documentation purposes
     * @param name - replacement text
     * @return closure that modifies each line with the appropriate replacement text
     */
    public Closure getCmdLogClosure(final String name) {
        return { String line -> line?.replaceAll('java .*.jar', name) } // cleans up log file for documentation generation
    }

    /**
     * Easier for CLI tasks to define action and a name extension - this avoids a lot of duplication in task definitions
     * Example:  action: 'addPage', ext: 'withParent' -> results in name: 'addPageWithParent'
     * @return closure that takes a task and sets its name field (if not defined) based on action and ext
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Closure getAddTaskClosure() {
        return { Map map ->
            if (helper.isBlank(map.name)) {
                map.name = (map.action ?: '') + (map.ext != null ? map.ext : '') // set the name in the map as well as return the name
            }
            return map.name // return name
        }
    }

    /**
     * Get CLI client information
     * @return map of client information
     */
    public Map<String, Object> getClientInfo(final inCli = null) {
        return getClientInfo(inCli instanceof Map ? inCli : [cli: inCli])
    }

    public Map<String, Object> getClientInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {
            def cmd = cli.trim() + ' --action getClientInfo --outputFormat 2'
            def p = gint.getParallelHelper().shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()
            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]
                info.cli = cli

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing

                info.clientName              = helper.getPrettyFormatValue(string, 'client name')
                info.clientVersion           = helper.getPrettyFormatValue(string, 'client version')
                info.clientDescription       = helper.getPrettyFormatValue(string, 'client description')
                info.clientUsage             = helper.getPrettyFormatValue(string, 'client usage')
                info.clientBuildDate         = helper.getPrettyFormatValue(string, 'client build date')
                info.clientBuildRevision     = helper.getPrettyFormatValue(string, 'client build revision')
                info.clientJavaCompatibility = helper.getPrettyFormatValue(string, 'client java compatibility')

                info.osName                  = helper.getPrettyFormatValue(string, 'os name')
                info.osVersion               = helper.getPrettyFormatValue(string, 'os version')

                info.javaName                = helper.getPrettyFormatValue(string, 'java name')
                info.javaVersion             = helper.getPrettyFormatValue(string, 'java version')
                info.javaArguments           = helper.getPrettyFormatValue(string, 'java arguments')
                info.usedMemory              = helper.getPrettyFormatValue(string, 'used memory \\(MB\\)')
                info.freeMemory              = helper.getPrettyFormatValue(string, 'free memory \\(MB\\)')
                info.totalMemory             = helper.getPrettyFormatValue(string, 'total memory \\(MB\\)')
                info.maxMemory              = helper.getPrettyFormatValue(string, 'max memory \\(MB\\)')
            } else if (gint.getVerbose()) {
                //addMessage 'debug', "rc: ${rc}"
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', 'Configuration missing: cli or similar properties are blank or not defined.'
        }
        if (gint.getVerbose()) {
            if (info) {
                helper.log('clientInfo', info)
            } else {
                helper.log('cli', cli)
            }
        }
        return info
    }

    /**
     * Return the cli that will be used
     * @return
     */
    public String getCli(final inCli = null) {
        return helper.getWithExtendedLookup(inCli ?: helper.getParameterValue('cli'), 'acli')
    }

    /**
     * Get standard CLI help task. Runs the help action and makes sure every action being tested is found in the output of the help action
     * @return task
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected GintTask addHelpTextTask(final ignoreList = []) {

        def actionList = [] as Set
        gint.project.getTasks().each { org.gradle.api.Task entry ->
            if (entry instanceof GintTask) {
                GintTask task = (GintTask) entry
                if (task.closures.inline == null) {
                    if ((task.map?.render == null) && (task.map?.action != null) && (task.map.action instanceof String)) {
                        actionList << task.map.action
                    }
                }
            }
        }
        actionList = actionList - ignoreList  // remove unwanted entries

        // check the data to ensure all actions defined have help text
        Map<String, Object> map = [
            name: 'helpText',  // Gradle 5.0 does not allow help as a task name
            action: 'help',
            redirect: "2> ${gint.getOutputFile('helpText')}",
            parameters: [
                help: null,
            ],
            expected: -2, // help is a -2 exit code
            output: [
                data: [
                    actionList,
                ],
            ],
        ]
        return gint.taskHelper.add(map)
    }

    /**
     * Look for an id in data, looks for pattern like (12345) or id: 12345 or id 12345 or id=12345 or . : 12345
     * @param data - data to be searched
     * @param occurrance - 1 based, 1 is first occurrence (default), 2 is second occurrence
     * @return id or null
     */
    static public String searchForId(final data, final int occurrence = 1) {
        def matcher = (data.toString() =~ /(?:\((\d+)\))|(?:id[:=]{0,1} {0,1}(\d+)|(?:\. : (\d+)))/)
        for (int i = 1; i < occurrence && matcher.find(); i++) { // skip earlier occurrences
        }
        return (matcher.find() ? matcher.group(1) ?: matcher.group(2) ?: matcher.group(3) : null)
    }

    /**
     * Look for a url in data, looks for pattern like ://
     * @param data - data to be searched
     * @param occurrance - 1 based, 1 is first occurrence (default), 2 is second occurrence
     * @return url or null
     */
    static public String searchForUrl(final data, final int occurrence = 1) {

        def matcher = (data.toString() =~ "(\\S+://\\S+)")
        for (int i = 1; i < occurrence && matcher.find(); i++) {
        }
        String result = (matcher.find() ? matcher.group(1) : null)
        if (result.endsWith(']') && (data instanceof Collection)) {
            return result.substring(0, result.size() - 1) //ignore trailing ] if data is a list to avoid toString format
        }
        return result
    }
}
