/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.plugin.Gint

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

/**
 * Gint extensions for Service Desk
 */
@CompileStatic
@TypeChecked
class ServiceDeskHelper extends JiraHelper {

    /**
     * Constructor
     */
    ServiceDeskHelper(final Gint gint) {
        super(gint, 'servicedesk')
    }


    /**
     * Contact server and retrieve information.
     * Be as generic as possible on output scanning to handle CLI changes - ignore case and spacing.
     * <pre>
     * Expected output: "JIRA version: 4.2, build: 587, edition: Enterprise, time: 2010-10-26T16:07:09.329-0500, time zone: America/Chicago"
     * </pre>
     * @return Map with version, build, and time zone. Return null if jira can not be contacted.
     */
    public Map<String, Object> getServerInfo(final inCli = null) {
        return getServerInfo(inCli instanceof Map ? inCli : [cli: inCli])
    }

    @Override
    public Map<String, Object> getServerInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {
            def cmd = cli.trim() + ' --action getServerInfo --outputFormat 2 --dateFormat "' + helper.getJsonDateFormatString() + '"'
            def p = gint.getParallelHelper().shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()
            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]
                info.cli = cli

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing

                info.product           = helper.getPrettyFormatValue(string, 'product')
                info.hosting           = helper.getPrettyFormatValue(string, 'hosting')
                info.version           = helper.getPrettyFormatValue(string, 'version')
                info.platformVersion   = helper.getPrettyFormatValue(string, 'platform version')
                info.productIsLicensed = helper.getPrettyFormatValue(string, 'product is licensed')
                info.url               = helper.getPrettyFormatValue(string, 'base url') // different than others
                info.title             = helper.getPrettyFormatValue(string, 'title')
                info.build             = helper.getPrettyFormatValue(string, 'build')
                info.buildDate         = helper.getPrettyFormatValue(string, 'build date')
                info.time              = helper.getPrettyFormatValue(string, 'current time')
                //info.timeZone          = helper.getPrettyFormatValue(string, 'time zone')
                info.connectorVersion  = helper.getPrettyFormatValue(string, 'connector version')
                info.license           = helper.getPrettyFormatValue(string, 'license information')
                info.client            = helper.getPrettyFormatValue(string, 'client')
                info.user              = helper.getPrettyFormatValue(string, 'user')
                info.userKey           = helper.getPrettyFormatValue(string, 'user key')          // CLI 9.6
                info.userDisplayName   = helper.getPrettyFormatValue(string, 'user display name') // CLI 9.6

                // note we check the platform version
                // also check for valid version format (including a period)
                // as cloud started returning bad data - see GINT-203
                //info.isCloud = helper.compareSeparatedGreaterThanOrEqual(info.platformVersion.toString(), '1000') || !info.platformVersion?.toString()?.contains('.')

                info.isCloud           = info.hosting == 'cloud' || ((String)info.client)?.contains('cloud') || helper.compareSeparatedGreaterThanOrEqual(info.platformVersion.toString(), '1000') || !info.platformVersion?.toString()?.contains('.')
                info.isServer          = info.hosting == 'server'
                info.isDatacenter      = info.hosting == 'datacenter'

                if (info.url != null) {
                    def matcher = info.url =~ /\S*\/\/[^\/]*(\S*)/
                    info.contextPath = (matcher.matches()) ? matcher.group(1) ?: "" : ""
                }

                helper.removeNullValues(info)

                if (info.product == null) {
                    p.printOutList()
                    addMessage 'warning', 'Could not determine server information from the data returned from the request.'
                }

            } else if (gint.getVerbose()) {
                //message 'debug', "rc: ${rc}"
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', "Configuration missing: cli and servicedeskCli property is blank or not defined."
        }
        if (gint.getVerbose()) {
            if (info) {
                helper.log('serverInfo', info)
                helper.log('projectKey', getProjectKey()) // show default project key
                helper.log('projectName', getProjectName()) // show default project key
            } else {
                helper.log('cli', cli)
            }
        }
        return info
    }
}
