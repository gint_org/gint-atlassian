/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.plugin.Gint
import org.gint.tasks.GintTask

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Gint extensions for Bamboo
 */
@CompileStatic
@TypeChecked
class BambooHelper extends AtlassianHelper {

    protected String projectKey  // project key

    /**
     * Constructor
     */
    BambooHelper(final Gint gint) {
        super(gint, 'bamboo')
    }

    /**
     * Get a generated project name
     * @return a project name
     */
    public String getProjectName() {
        return gint.getConstructName()
    }

    /**
     * Set project name
     * @param project name
     */
    public void setProjectName(final String name) {
        gint.setConstructName(name)
    }

    /**
     * Get a generated project name
     * @return a project name
     */
    public String getProjectKey() {
        if (projectKey == null) {
            // provide a default key if not set
            projectKey = getProjectName().replace(' ', '').toUpperCase()
        }
        return projectKey
    }

    /**
     * Set project key
     * @param project key
     */
    public void setProjectKey(final key) {
        projectKey = key
    }

    /**
     * Create standard project - not needed for Bamboo
     * Projects are created by creating a plan and specifying a new project.
     */

    protected GintTask addDeleteProjectTask(final Map parameters = null) {
        gint.taskHelper.addTearDown(getDeleteProjectMap(parameters))
    }

    /**
     * Return a task that deletes a standard project
     * @param postfix - added to standard project name
     * @return task
     */
    protected Map<String, Object> getDeleteProjectMap(final Map parameters = null) {
        def postFix = parameters?.postFix ?: parameters?.postfix ?: ''
        return [
            action: 'deleteProject', ext: postFix,
            parameters: [
                project: getProjectKey() + postFix,
            ],
        ] as Map
    }

    /**
     * Return a task that deletes set of projects matching regex pattern
     * Task is successful if ALL projects are deleted successful. Fails if any one project is not deleted.
     * Attempt is made to delete all projects before failure
     * @param name - name of task
     * @param regex - Regex pattern (~/..../) to match project names - defaults to all projects starting with standard project name
     * @param excludeList - list of project keys NOT to be removed
     * @return task
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected GintTask addDeleteProjectsTask(final Map parameters = null) {

        List excludeList = []
        parameters.excludeList.each() { String entry ->
            excludeList << entry?.toUpperCase()  // make sure all entries are upper case in order to match project keys
        }

        gint.taskHelper.addTearDown([
            name: parameters?.name ?: 'deleteProjects',
            action: 'getProjectList',
            file: true,  // write list out to a file
            parameters: [
                regex: getProjectKey() + '.*', // subset list to only projects starting with project key
                columns: 'key',  // just need the project key
            ],  // get a list of projects by key
            output: [:], // need this to not be null to get file output data later
            successClosure: { GintTask task ->
                def projectLines = helper.readFileToList(task.output.file)

                def projectList = []
                projectLines.each { line ->
                    def key = helper.stripQuotes(line, '"')
                    if (!(key.startsWith('Key')) && !(key in excludeList)) {
                        projectList << key
                    }
                }
                helper.log("projects to delete", projectList)

                boolean result = true
                projectList.each { entry ->

                    def cmd = task.cmd.replace('getProjectList', 'deleteProject --project ' + entry)
                    //cmd = helper.removeJsapParameter('file', cmd)
                    //cmd = helper.removeJsapParameter('regex', cmd)
                    //cmd = helper.removeJsapParameter('columns', cmd)

                    result = helper.runCmd(cmd: cmd, log: true) && result
                }
                return result // fail test if any delete was not successful, but do as many as possible
            }
        ])
    }

    /**
     * Check for server configuration and availability.
     * @param cli - cli to use, otherwise it will default to standard confluence cli lookup
     * @return true if cli is configured and server is available and set parameters.info to the server info
     */
    public boolean isServerAvailable(final Map parameters = null) {
        def cli = getCli(parameters?.cli)  // use to verify cli is configured and server is available
        def info = getServerInfo(cli)  // use to verify cli is configured and server is available
        boolean result = (info != null)
        if (parameters != null) {
            parameters.info = info
        }
        return result
    }

    /**
     * Contact server and retrieve information.
     * Be as generic as possible on output scanning to handle CLI changes - ignore case and spacing.
     * <pre>
     * Expected output: "JIRA version: 4.2, build: 587, edition: Enterprise, time: 2010-10-26T16:07:09.329-0500, time zone: America/Chicago"
     * </pre>
     * @return Map with version, build, and time zone. Return null if bamboo can not be contacted.
     */
    public Map<String, Object> getServerInfo(final inCli = null) {
        return getServerInfo(inCli instanceof Map ? inCli : [cli: inCli])
    }

    public Map<String, Object> getServerInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {
            def cmd = cli.trim() + ' --action getServerInfo --outputFormat 2 --dateFormat "' + helper.getJsonDateFormatString() + '"'
            def p = gint.getParallelHelper().shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()
            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]
                info.cli = cli
                int index = 0

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing

                info.product           = helper.getPrettyFormatValue(string, 'product')
                info.hosting           = helper.getPrettyFormatValue(string, 'hosting')
                info.version           = helper.getPrettyFormatValue(string, 'version')
                info.productIsLicensed = helper.getPrettyFormatValue(string, 'product is licensed')
                info.url               = helper.getPrettyFormatValue(string, 'base url') // different than others
                info.title             = helper.getPrettyFormatValue(string, 'title')
                info.build             = helper.getPrettyFormatValue(string, 'build')
                info.buildDate         = helper.getPrettyFormatValue(string, 'build date')
                info.time              = helper.getPrettyFormatValue(string, 'current time')
                info.timeZone          = helper.getPrettyFormatValue(string, 'time zone')
                info.connectorVersion  = helper.getPrettyFormatValue(string, 'connector version')
                info.license           = helper.getPrettyFormatValue(string, 'license information')
                info.client            = helper.getPrettyFormatValue(string, 'client')
                info.user              = helper.getPrettyFormatValue(string, 'user')

                // Needs CLI 9.2 or higher for hosting
                info.isServer          = info.hosting == 'server'

                if (info.url != null) {
                    def matcher2 = info.url =~ /\S*\/\/[^\/]*(\S*)/
                    info.contextPath = (matcher2.matches()) ? matcher2.group(1) ?: "" : ""
                }

                helper.removeNullValues(info)

                if (info.product == null) {
                    p.printOutList()
                    addMessage 'warning', 'Could not determine server information from the data returned from the request.'
                }

            } else if (gint.getVerbose()) {
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', 'Configuration missing: cli and bambooCli properties are blank or not defined.'
        }
        if (info && parameters?.newProject == true) {
            setProjectKey(getProjectKeyVerifyNewWithDelete(cli: info?.cli))
        }
        if (gint.getVerbose()) {
            if (info) {
                helper.log('serverInfo', info)
                helper.log('projectKey', getProjectKey()) // show default project key
                helper.log('projectName', getProjectName()) // show default project key
            } else {
                helper.log('cli', cli)
            }
        }
        return info
    }

    /**
     * Return the cli that will be used
     * @return
     */
    public String getCli(final inCli = null) {
        return helper.getWithExtendedLookup(inCli ?: helper.getParameterValue('cli'), 'bambooCli', 'acli bamboo')
    }

    protected String getProjectKeyVerifyNewWithDelete(final Map parameters = null) {
        def cli = parameters?.cli.toString() ?: getCli().trim()
        def postfixList = [
            '1',
            '2',
            '3',
            '4',
            '5'
        ]
        Collections.shuffle(postfixList) // random list to avoid duplicates for multiple concurrent runs

        def key
        def doDelete = false
        for (def postfix in (['']+ postfixList)) {
            key = getProjectKey() + postfix
            def cmd =  cli + ' -a getProject --project ' + key
            if (helper.runCmd(cmd: cmd, log: false, logOutput: false)) {
                doDelete = true
            } else {
                break
            }
        }
        doDelete = true // temp
        if (doDelete) { // delete all possible ones revert to the original key if n
            if (key == null) {
                key = getProjectKey()
            }
            def cmd = /${cli} -a runFromList --list "' ',1,2,3,4,5" -i "-a deleteProject --project ${getProjectKey()}@entry@ --continue" --quiet --continue/
            helper.runCmd(cmd: cmd, log: true, logOutput: false)
        }
        return key
    }

    // Note: for these searches, we use negative look ahead and look behind to ensure the entity
    //       is not embedded in characters or with a -. Other punctuation is ok.

    /**
     * Look for build key in data
     * @param data - where to look for  key
     * @return key or null if not found
     */
    static public String searchForBuildKey(final data) {
        def matcher = (data =~ /(?<![-\w])((?:[a-zA-Z][a-zA-Z0-9]+-){2}\d+)(?![-\w])/)
        return (matcher.find() ? matcher.group(1) : null)
    }

    /**
     * Look for build key in data
     * @param data - where to look for issue key
     * @return issue key or null if not found
     */
    static public String searchForJobKey(final data) {
        def matcher = (data =~ /(?<![-\w])((?:[a-zA-Z][a-zA-Z0-9]+-){3}\d+)(?![-\w])/)
        return (matcher.find() ? matcher.group(1) : null)
    }

    /**
     * Look for issue key in data
     * @param data - where to look for issue key
     * @return issue key or null if not found
     */
    static public String searchForBuildNumber(final data) {
        def matcher = (data =~ /(?<![-\w])(?:[a-zA-Z][a-zA-Z0-9]+-){2,3}(\d+)(?![-\w])/)
        return (matcher.find() ? matcher.group(1) : null)
    }
}
