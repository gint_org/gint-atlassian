/*
 /*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.plugin.Gint

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Gint extensions for Bitbucket
 */
@CompileStatic
@TypeChecked
class BitbucketHelper extends AtlassianHelper {

    protected String projectKey  // project key

    /**
     * Constructor
     */
    BitbucketHelper(final Gint gint) {
        super(gint, 'bitbucket')
    }

    /**
     * Get a generated project name
     * @return a project name
     */
    public String getProjectName() {
        return gint.getConstructName()
    }

    /**
     * Set project name
     * @param project name
     */
    public void setProjectName(final String name) {
        gint.setConstructName(name)
    }

    /**
     * Get a generated project name
     * @return a project name
     */
    public String getProjectKey() {
        if (projectKey == null) {
            // provide a default key if not set
            projectKey = getProjectName().replace(' ', '').toUpperCase()
        }
        return projectKey
    }

    /**
     * Set project key
     * @param project key
     */
    public void setProjectKey(final key) {
        projectKey = key
    }

    /**
     * Add standard project
     * @return map
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected Map<String, Object> getCreateProjectMap(final String postFix = "") {
        return [
            action: 'createProject', ext: postFix,
            project: getProjectKey() + postFix,
            parameters: [
                name: getProjectName() + postFix,
            ],
            tearDown: 'deleteProject' + postFix,
        ]
    }

    /**
     * Remove standard project
     * @return map
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected Map<String, Object> getDeleteProjectMap(final String postFix = "") {
        return [
            action: 'deleteProject', ext: postFix,
            parameters: helper.jsapParameter('project', getProjectKey() + postFix),
        ]
    }

    /**
     * Contact server and retrieve information.
     * Be as generic as possible on output scanning to handle CLI changes - ignore case and spacing.
     * <pre>
     * Expected output: "JIRA version: 4.2, build: 587, edition: Enterprise, time: 2010-10-26T16:07:09.329-0500, time zone: America/Chicago"
     * </pre>
     * @return Map with version, build, and time zone. Return null if server can not be contacted.
     */
    public Map<String, Object> getServerInfo(final inCli = null) {
        return getServerInfo(inCli instanceof Map ? inCli : [cli: inCli])
    }

    @Override
    public Map<String, Object> getServerInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {
            def cmd = cli.trim() + ' --action getServerInfo --outputFormat 2 --dateFormat "' + helper.getJsonDateFormatString() + '"'
            def p = gint.getParallelHelper().shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()
            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]
                info.cli = cli
                int index = 0

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing

                info.product           = helper.getPrettyFormatValue(string, 'product')
                info.hosting           = helper.getPrettyFormatValue(string, 'hosting')
                info.version           = helper.getPrettyFormatValue(string, 'version')
                info.productIsLicensed = helper.getPrettyFormatValue(string, 'product is licensed')
                info.url               = helper.getPrettyFormatValue(string, 'base url') // different than others
                info.title             = helper.getPrettyFormatValue(string, 'title')
                info.build             = helper.getPrettyFormatValue(string, 'build')
                info.buildDate         = helper.getPrettyFormatValue(string, 'build date')
                info.time              = helper.getPrettyFormatValue(string, 'current time')
                info.timeZone          = helper.getPrettyFormatValue(string, 'time zone')
                info.connectorVersion  = helper.getPrettyFormatValue(string, 'connector version')
                info.license           = helper.getPrettyFormatValue(string, 'license information')
                info.client            = helper.getPrettyFormatValue(string, 'client')
                info.user              = helper.getPrettyFormatValue(string, 'user')
                info.userKey           = helper.getPrettyFormatValue(string, 'user key')          // CLI 9.6
                info.userDisplayName   = helper.getPrettyFormatValue(string, 'user display name') // CLI 9.6

                // Needs CLI 9.2 or higher for hosting
                info.isCloud           = ((String)info.client)?.contains('cloud')
                info.isServer          = info.hosting == 'server'
                info.isDatacenter      = info.hosting == 'datacenter'

                if (info.url != null) {
                    def matcher2 = info.url =~ /\S*\/\/[^\/]*(\S*)/
                    info.contextPath = (matcher2.matches()) ? matcher2.group(1) ?: "" : ""
                }

                helper.removeNullValues(info)

                if (info.product == null) {
                    p.printOutList()
                    addMessage 'warning', 'Could not determine server information from the data returned from the request.'
                }

            } else if (gint.getVerbose()) {
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', "Configuration missing: cli and bitbucketCli properties are blank or not defined."
        }
        if (gint.verbose) {
            if (info) {
                helper.log('serverInfo', info)
                //helper.log('projectKey', getProjectKey()) // show default project key
                //helper.log('projectName', getProjectName()) // show default project key
            } else {
                helper.log('cli', cli)
            }
        }
        return info
    }

    /**
     * Return the cli that will be used
     * @return
     */
    public String getCli(final inCli = null) {
        return helper.getWithExtendedLookup(inCli ?: helper.getParameterValue('cli'), 'bitbucketCli', 'acli bitbucket')
    }
}
