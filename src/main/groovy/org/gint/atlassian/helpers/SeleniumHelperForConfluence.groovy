/* Copyright (c) 2009, 2019 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.plugin.Gint
import org.gint.tasks.GintTask
import org.openqa.selenium.WebElement

/**
 * Extend SeleniumHelper for Confluence specific methods. Otherwise use just like seleniumHelper.
 */
//@CompileStatic
//@TypeChecked
class SeleniumHelperForConfluence extends SeleniumHelperForAtlassian {

    final protected String CONNECT_MACRO_CLASS_NAME = 'conf-macro'      // Defined by Atlassian and could change but haven't
    final protected String CONNECT_MACRO_ATTRIBUTE = 'data-macro-name'  // Defined by Atlassian and could change but haven't

    final protected int CONNECT_RETRY_COUNT = 7

    /**
     *  Extension of selenium helper in a atlassian context
     */
    SeleniumHelperForConfluence(final Gint gint) {
        super(gint)
    }

    /**
     * Get the default application name.
     * @return application name
     */
    public String getDefaultApplication() {
        return 'confluence'
    }

    /**
     * Goto a Confluence page - requires GintForConfluence
     * @param parameters including driver or base, space, title or name, id, and optionally screenshot
     */
    public void gotoPage(final Map parameters = null) {

        def space = parameters?.space ?: gint.getConfluenceHelper().getSpaceKey()
        def title = parameters?.title ?: parameters.name

        Map pageInfo = gint.getConfluenceHelper().getPageInfo([
            space: space,
            title: title,
        ])

        if (pageInfo) {
            if (parameters?.verbose) {
                helper.log('pageInfo', pageInfo)
            }
            gotoUrl([
                driver: parameters?.driver ?: driver,
                url: helper.appendRequestParameters(pageInfo.url, parameters?.requestParameters),
                screenshot: parameters?.screenshot,
                name: parameters?.name ?: title,
                acceptAlert: parameters?.acceptAlert,
                dismissAlert: parameters?.dismissAlert,
            ])
        } else {
            def error = 'Go to page failed. See other messages for details.'
            if (parameters.throwException == true) {
                throw new Exception(error)
            }
            gint.addFailedMessage(error, false) // false to not cause test error
        }
    }

    /**
     * Goto a macro browser. Assumes you are on an edit screen (for example, see gotoPageEdit
     * @param macro - name of macros
     * @return located in the macro browser for the macro requested
     */
    public void gotoMacroBrowser(final Map parameters = null) {
        def element = findElement(id: 'rte-button-insert', clickable: true)
        clickWithRetry(element)
        delay()

        //element = findElement(base: element, id: 'rte-insert-macro', visible: true)
        element = findElement(base: element, text: 'Other ', visible: true) // covers either Other Macros or Other macros (5.5)
        assert element // find the open macros element for the macro browser - text starts with Other
        element.click()
        delay() // macro browser should come up
    }

    /**
     * Goto a macro browser. Assumes you are on an edit screen (for example, see gotoPageEdit
     * @param macro - name of macros
     * @return located in the macro browser for the macro requested
     */
    public void rteInsertMenuItem(final Map parameters = null) {
        def insertMenu = findAndClickElement(id: 'rte-button-insert', visible: true)
        delay()
        findAndClickElement(base: insertMenu, text: parameters.text, visible: true)
        delay() // macro browser should come up
    }

    /**
     * Goto a specific macro in the macro browser. Assumes you are on the macro browser screen (for example, see gotoMacroBrowser)
     * @param macro - name of macros
     * @param category - if a valid category is specified, macro must be found on the category list
     * @return located in the macro browser for the macro requested
     */
    public void gotoMacroBrowserMacro(final Map parameters) {
        def element = findMacroBrowserMacro(parameters)
        assert element // macro found?
        element.click()  // goto the macro itself
        delay()
    }

    /**
     * Goto a specific macro in the macro browser. Assumes you are on the macro browser screen (for example, see gotoMacroBrowser)
     * @param macro - name of macros
     * @param category - if a valid category is specified, macro must be found on the category list
     * @return located in the macro browser for the macro requested
     */
    public WebElement findMacroBrowserMacro(final Map parameters) {
        assert parameters?.macro // macro is a required parameter
        def categoryId = parameters.category ? getMacroBrowserCategoryId(parameters.category) : ''  // all is the same as ''
        def categoryPrefix = categoryId == '' ? '' : categoryId + '-'

        def element = findElement(id: categoryPrefix + 'macro-' + parameters.macro, visible: true)

        return element
    }

    /**
     * Goto a Confluence page in edit mode
     */
    public void gotoPageEdit(final Map parameters) {
        gotoPage(parameters)

        handleAlert(acceptAlert: true)
        def element = findElement(id: 'editPageLink', clickable: true)
        assert element // found edit page link
        element.click()
        delay()
    }

    // in the macro browser, locate to our macro
    /**
     * In the macro browser, go to a page and open the macro browser on the macro you want
     * @param space
     * @param title
     * @param macro
     */
    public void gotoMacro(final Map parameters) {
        //cancelEdit() // just in case previous edit was open, otherwise get a alert - too much of delay, user should do this if needed
        assert parameters != null // gotoMacro parameters cannot be null
        gotoPageEdit([
            space: parameters.space,
            title: parameters.title,
            throwException: parameters.throwException ?:true, // default to true since subsequent requests will fail with assert failure anyway
        ])
        gotoMacroBrowser()
        gotoMacroBrowserMacro(macro: parameters.macro)
    }

    /**
     * Progressively type text into search field. Assumes you are on the macro browser screen (for example, see gotoMacroBrowser)
     */
    public void progresiveSearchInMacroBrowser(final Map parameters) {
        assert parameters?.text // text to search for, could be a partial macro name for instance

        def element = findElement(id: 'macro-browser-search', visible: true)
        assert element // found search box
        parameters.text.each { character ->
            element.sendKeys(character) // search for our macro
            screenshot()
            delay()
        }
    }

    /**
     * Goto a macro browser category. Assumes you are on the macro browser screen (for example, see gotoMacroBrowser)
     */
    public void gotoMacroBrowserCategory(final Map parameters) {
        assert parameters?.category // category is a required parameter

        def element = findElement(id: 'category-button-' + getMacroBrowserCategoryId(parameters.category), visible: true)
        assert element // found category
        element.click()
        screenshot()
        delay()
    }

    /**
     * Get the macro browser id for the category provided in normal UI terms. Only valid id should be specified, but we don't verify as the list may vary over time
     */
    public String getMacroBrowserCategoryId(def category) {
        // map category to id where it differs from being the same
        def categoryMap = [ // others are all, communication, confluence-content, formatting, media, navigation, reporting, visuals
            all: '', // all is not really a category
            administration: 'admin',
            hidden: 'hidden-macros',
            'visuals & images': 'visuals',
        ]
        category = category.toString().trim().toLowerCase().replace(' ', '-')
        def id = categoryMap[category] ?: category // use the mapping if necessary
        return id
    }

    /**
     * Cancel an macro edit session if it is already open, otherwise nothing.
     */
    public void cancelMacroEdit() {
        def element = isLegacyEditor() //
                        ? findElement(id: 'rte-button-cancel', clickable: true) //
                        : findElement(cssSelector: '#macro-details-page > div.dialog-button-panel > a.button-panel-link.button-panel-cancel-link', clickable: true)
        clickWithRetry(element)
    }

    /**
     * Cancel an edit session if it is already open, otherwise nothing.
     */
    public void cancelEdit() {
        def element = findElement(id: 'rte-button-cancel', clickable: true)
        clickWithRetry(element)
        if (!isLegacyEditor()) {
            element = findElement(id: "qed-discard-button", visible: true)
            if (element) {
                element.click()
            }
            // otherwise ignore
        }
    }

    protected void clickWithRetry(element) {
        if (element) {
            for (int i = 0; i <= 10; i++) {
                if (element != null) {
                    try {
                        element.click()
                        break
                    } catch(Exception e) { // getting a not-clickable error at least the first time for macro browser - strange
                        if (i == 5) {
                            message 'info', 'Waiting ... ' + element
                        }
                        sleep(100)
                        if (i == 10) {
                            message 'warning', e
                        }
                    }
                }
            }
        }
    }

    public boolean isLegacyEditor() {
        helper.log("info", getServerInfo())
        return (getServerInfo() != null) && !helper.compareSeparatedGreaterThanOrEqual(getServerInfo()?.version, '6.0')
    }

    /**
     * Switch driver to connect iframe associate to a macro given by name. Accepts only named parameters
     * @param macro - required
     * @param verbose - optional
     * @param index of macro - 0 for first macro matching criteria
     * @return non null if frame found and switched, null if an error occurred and no switch
     */
    protected WebElement switchToConnectMacroFrame(Map map) {

        assert map?.macro != null // requires macro be provided
        //helper.log 'map', map
        delay()

        def element = null
        for (int i = 0; (i < CONNECT_RETRY_COUNT) && (element == null); i++) {
            //println '\nTry #' + i
            sleep(i > 2 ? 3000 : 1000) // increase sleep time after a couple of failures to limit number of loops
            try {
                element = findConnectMacroFrame(map, i == CONNECT_RETRY_COUNT - 1) // only show errors on last retry loop
            } catch (Exception exception) {
                helper.dLog 'find connect frame attempt ' + i + 1, exception
            }

            try {
                if (element != null) {
                    helper.dLog('iframe element', element)
                    map.frame = element // iframe element we are switching to
                    // Set to iframe
                    getDriver().switchTo().frame(element)
                    delay()  // make sure we pause after the iframe content is rendered if the delay parameter is set
                }
            } catch (Exception exception) {
                helper.dLog 'find connect iframe switch attempt ' + i + 1, exception
                element = null
            }
        }
        if (element == null) {
            message 'error', 'Attempts to find connect macro iframe were unsuccessful. No rendered data available.'
        }
        return element
    }

    protected WebElement findConnectMacroFrame(Map map, final boolean showErrorMessage = true) {

        def number = map.macroNumber ?: 1 // default to first macro on page, this is 1-based
        def verbose = map.verbose == true

        // Main content
        def element = findElement([
            id: 'main-content',
            present: true, // ADG3 needs a wait before the element is available
            wait: getWait(map.wait),
            verbose: verbose,
        ])
        def screenshot = map.screenshot != null ? map.screenshot : true // default to true if not specified

        if (element == null) {
            if (showErrorMessage) {
                message 'error', 'Not found element: main-content. No rendered data available.'
            }
        } else {

            // Confluence macro by name
            element = findElement([
                base: element,
                multiple: true, // get a list if available
                className: CONNECT_MACRO_CLASS_NAME, // Atlassian provided
                attribute: CONNECT_MACRO_ATTRIBUTE,  // Atlassian provided
                attributeValue: map?.macro,
                exact: true, // exact match on macro name
                present: true, // ADG3 needs a wait before the element is available
                wait: map.wait,
                screenshot: screenshot,
                verbose: verbose,
            ])
            //helper.log 'number', number
            //helper.log 'element class', element.class.name
            if (element instanceof List) {
                if (((List) element).size() >= number) {
                    element = element.get(number - 1) // element by macro number on page, usually 1 for first
                } else {
                    if (showErrorMessage) {
                        message 'error', 'Only ' + element.size() + ' macro elements found. ' + number + ' were requested.'
                    }
                    element = null
                }
            } else {
                element = null
            }
            if (element == null) {
                if (showErrorMessage) {
                    message 'error', 'Not found class ' + CONNECT_MACRO_CLASS_NAME + ' with attribute ' + CONNECT_MACRO_ATTRIBUTE + '. Macro ' + map?.macro + ' not found. No rendered data available.'
                }
            } else {
                // Macro's iframe

                //helper.log 'Macro element', element
                //helper.log 'map', map

                element = findElement([
                    base: element,
                    tag: 'iframe',
                    present: true,  // ADG3 needs a wait before the element is available
                    wait: map.wait,
                    screenshot: screenshot,
                    verbose: verbose,
                ])
                //helper.log 'element', element
            }
        }
        return element
    }

    /**
     * Find and get table body text as 2 dimensional list or null if not found or other problem accessing
     * - uses same parameters as findElement
     * - includeId - if true, then the row id attribute will be included
     * This handles connect or not-connect pages depending on frame parameters normally available via frame in standard test tasks
     * @return null or list of rows, each row being a list of column values
     */
    public List<List<String>> getTableOnPage(final GintTask task) {
        assert task.map != null  // map must be provided
        def table

        switchAndRunClosure(task, { GintTask t ->
            //helper.log 'map', t.map
            if (t.map.frame != null) {
                table = getTable(t.map.parameters)
            } else {
                gotoPage([
                    space: t.map.space,
                    title: t.map.title,
                    throwException: true,
                ])
                table = getTable(t.map.parameters)
            }
        })
        return table
    }
}
