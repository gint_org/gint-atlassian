/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.helpers.ParallelHelper
import org.gint.plugin.Gint
import org.gint.tasks.GintTask

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Gint extensions for Jira
 */
@CompileStatic
@TypeChecked
class JiraHelper extends AtlassianHelper {

    protected String projectKey  // project key

    /**
     * Constructor
     */
    JiraHelper(final Gint gint, final String name = 'jira') {
        super(gint, name)
    }

    /**
     * Get a generated project name
     * @return a project name
     */
    public String getProjectName() {
        return gint.getConstructName()
    }

    /**
     * Set project name
     * @param project name
     */
    public void setProjectName(final String name) {
        gint.setConstructName(name)
    }

    /**
     * Get a generated project name
     * @return a project name
     */
    public String getProjectKey() {
        if (projectKey == null) {
            // provide a default key if not set
            projectKey = getProjectName().replace(' ', '').toUpperCase()
        }
        return projectKey
    }

    /**
     * Set project key
     * @param project key
     */
    public void setProjectKey(final key) {
        projectKey = key
    }

    /**
     * Limit the size of the project key. Later versions of JIRA limit to 10, but there are discussions about increasing that
     */
    public void limitProjectKey(int length = 10) {
        setProjectKey(helper.abbreviateText(getProjectKey(), length))
    }

    public GintTask addCreateProjectTask(final Map map = null) {
        return (GintTask) gint.taskHelper.addSetUp(getCreateProjectTaskMap(map))
    }

    /**
     * Create standard Jira project
     * @param postFix - defaults to blank
     * @param parameters - defaults to [] and should override default settings where provided
     * @return map
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Map<String, Object> getCreateProjectTaskMap(final Map map = null) {
        def postFix = map?.postFix ?: map?.postfix ?: ''
        def name = 'createProject' + (map?.project ?: postFix)
        return [
            action: 'createProject', name: name,
            level: map?.level,
            dependsOn: map?.dependsOn ?: map?.depends ?: null,
            mustRunAfter:  map?.mustRunAfter ?: map?.mustRunAfter ?: null,
            ignoreDependsResult: map?.ignoreDependsResult ?: false,
            project: map?.project ?: getProjectKey() + postFix,
            parameters: [
                name: map?.project ?: getProjectName() + postFix,
                lead: 'automation',
            ] + (map?.parameters ?: [:]),
            tearDown: name.replace('create', 'delete'),
        ] + (map?.cmdGenerator ? [cmdGenerator: map?.cmdGenerator] : [:])
    }

    public GintTask addDeleteProjectTask(final Map map = null) {
        return (GintTask) gint.taskHelper.addTearDown(getDeleteProjectTaskMap(map))
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    public Map<String, Object> getDeleteProjectTaskMap(final Map map = null) {
        def postFix = map?.postFix ?: map?.postfix ?: ''
        return [
            action: 'deleteProject', name: 'deleteProject' + (map?.project ?: postFix),
            dependsOn: map?.dependsOn ?: map?.depends ?: null,
            mustRunAfter:  map?.mustRunAfter ?: map?.mustRunAfter ?: null,
            ignoreDependsResult: map?.ignoreDependsResult ?: false,
            parameters: [
                project: map?.project ?: getProjectKey() + postFix,
                continue: null,
            ]
            + (map?.parameters ?: [:]),
        ] + (map?.cmdGenerator ? [cmdGenerator: map?.cmdGenerator] : [:])
    }

    /**
     * Check for server configuration and availability.
     * @param cli - cli to use, otherwise it will default to standard confluence cli lookup
     * @return true if cli is configured and server is available and set parameters.info to the server info
     */
    public boolean isServerAvailable(final Map parameters = null) {
        def cli = getCli(parameters?.cli)  // use to verify cli is configured and server is available
        def info = getServerInfo(cli)  // use to verify cli is configured and server is available
        boolean result = (info != null)
        if (parameters != null) {
            parameters.info = info
        }
        return result
    }

    /**
     * Contact server and retrieve information.
     * Be as generic as possible on output scanning to handle CLI changes - ignore case and spacing.
     * <pre>
     * Expected output: "JIRA version: 4.2, build: 587, edition: Enterprise, time: 2010-10-26T16:07:09.329-0500, time zone: America/Chicago"
     * </pre>
     * @return Map with version, build, and time zone. Return null if jira can not be contacted.
     */
    public Map<String, Object> getServerInfo(final inCli = null) {
        return getServerInfo(inCli instanceof Map ? inCli : [cli: inCli])
    }

    @Override
    public Map<String, Object> getServerInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {
            def cmd = cli.trim() + ' --action getServerInfo --outputFormat 2 --dateFormat "' + helper.getJsonDateFormatString() + '"'
            def p = gint.getParallelHelper().shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()
            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]
                info.cli = cli

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing

                info.product           = helper.getPrettyFormatValue(string, 'product')
                info.hosting           = helper.getPrettyFormatValue(string, 'hosting')
                info.version           = helper.getPrettyFormatValue(string, 'version')
                info.productIsLicensed = helper.getPrettyFormatValue(string, 'product is licensed')
                info.url               = helper.getPrettyFormatValue(string, 'base url') // different than others
                info.title             = helper.getPrettyFormatValue(string, 'title')
                info.build             = helper.getPrettyFormatValue(string, 'build')
                info.buildDate         = helper.getPrettyFormatValue(string, 'build date')
                info.time              = helper.getPrettyFormatValue(string, 'current time')
                info.timeZone          = helper.getPrettyFormatValue(string, 'time zone')
                info.connectorVersion  = helper.getPrettyFormatValue(string, 'connector version')
                info.license           = helper.getPrettyFormatValue(string, 'license information')
                info.client            = helper.getPrettyFormatValue(string, 'client')
                info.user              = helper.getPrettyFormatValue(string, 'user')
                info.userKey           = helper.getPrettyFormatValue(string, 'user key')          // CLI 9.6
                info.userDisplayName   = helper.getPrettyFormatValue(string, 'user display name') // CLI 9.6
                info.userEmail         = helper.getPrettyFormatValue(string, 'user email')        // CLI 10.2 (if visible)

                // Needs CLI 9.2 or higher for hosting
                info.isCloud           = info.hosting == 'cloud' || ((String)info.client)?.contains('cloud') || helper.compareSeparatedGreaterThanOrEqual(info.version?.toString(), '1000')
                info.isServer          = info.hosting == 'server'
                info.isDatacenter      = info.hosting == 'datacenter'

                if (info.url != null) {
                    def matcher2 = info.url =~ /\S*\/\/[^\/]*(\S*)/
                    info.contextPath = (matcher2.matches()) ? matcher2.group(1) ?: "" : ""
                }

                helper.removeNullValues(info)

                if (info.product == null) {
                    p.printOutList()
                    addMessage 'warning', 'Could not determine server information from the data returned from the request.'
                }

            } else if (gint.getVerbose()) {
                //message 'debug', "rc: ${rc}"
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', "Configuration missing: cli and jiraCli properties are blank or not defined."
        }
        if (gint.getVerbose()) {
            if (info) {
                helper.log('serverInfo', info)
                helper.log('projectKey', getProjectKey()) // show default project key
                helper.log('projectName', getProjectName()) // show default project key
            } else {
                helper.log('cli', cli)
            }
        }
        return info
    }

    /**
     * Return the cli that will be used
     * @return
     */
    public String getCli(final inCli = null) {
        return helper.getWithExtendedLookup(inCli ?: helper.getParameterValue('cli'), 'jiraCli', 'acli jira')
    }

    public Map<String, Object> getUserInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {
            def cmd = cli.trim() + ' --action getUser ' + (helper.isNotBlank(parameters.userId) ? '--userId ' + parameters.userId : '')
            def p = gint.getParallelHelper().shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()
            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing

                info.user              = helper.getPrettyFormatValue(string, 'user id')  // CLI 10.2
                info.userKey           = helper.getPrettyFormatValue(string, 'user key')
                info.userDisplayName   = helper.getPrettyFormatValue(string, 'full name')
                info.userEmail         = helper.getPrettyFormatValue(string, 'email')

            } else if (gint.getVerbose()) {
                //message 'debug', "rc: ${rc}"
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', "Configuration missing: cli and jiraCli properties are blank or not defined."
        }
        return info
    }

    /**
     * Get the project key from an issue key
     * @param issue key
     * @return project key or null if not found
     */
    static public String getProjectFromIssue(final issue) {
        def matcher = (issue =~ /([a-zA-Z][a-zA-Z0-9]+)-\d+/)
        return (matcher.find() ? matcher.group(1) : null)
    }

    /**
     * Look for issue key in data
     * @param data - data to be searched
     * @param occurrance - 1 based, 1 is first occurrence (default), 2 is second occurrence
     * @return key or null
     */
    static public String searchForIssueKey(final data, final int occurrence = 1) {
        def matcher = (data.toString() =~ /(?<![-\w])([a-zA-Z][a-zA-Z0-9]+-\d+)(?![-\w])/)
        for (int i = 1; i < occurrence && matcher.find(); i++) {
        }
        return (matcher.find() ? matcher.group(1) : null)
    }

    /**
     * Look for the authorization token
     * @param data - where to look for token
     * @return token or null if not found
     */
    static public String searchForAtlassianToken(final data) {
        def matcher = (data.toString() =~ /<meta id="atlassian-token".*content="([^"]*)">/)
        return (matcher.find() ? matcher.group(1) : null)
    }

    /**
     * Look for the authorization token - first one found wins
     * @param file - where to look for token
     * @return token or null if not found
     */
    static public String searchForAtlassianToken(final File file) {
        def result = null
        file.eachLine { line ->
            if (result == null) {
                result = searchForAtlassianToken(line)
            }
        }
        return result
    }

    /**
     * Get the issue data.
     * @param parameters
     * @return list of issues, each issue is a map corresponding to csv output of getIssueList
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public List<Map<String, String>> getIssueList(final Map parameters) {
        assert parameters != null // getIssueList needs some parameters

        List<Map<String, String>> info = new ArrayList<Map<String, String>>()
        Map map = [   // need space and title or id
            action: 'getIssueList',
            parameters: new HashMap(parameters) + [
                outputFormat: parameters.outputFormat ?: '999', // by default, get all the data
                quiet: null, // just get the csv data
            ]
        ]
        if (map.parameters instanceof Map) {
            ((Map)map.parameters).remove('verbose')
            ((Map)map.parameters).remove('cmdGenerator')
        }
        //helper.log('map', map)

        //def cmdGenerator = parameters?.cmdGenerator ?: gint.getCmdGenerator()
        def cmd = gint.getCmdGeneratorHelper().getCmdForJira(parameters.cli ?: gint.getCmdGeneratorHelper().getCli('jira', parameters), map)
        ParallelHelper p = helper.runCmd(cmd: cmd, returnParallel: true, verbose: parameters.verbose == true)
        if (p.getReturnCode() == 0) {
            def list = []
            p.getOutList().each { row ->
                if (helper.isNotBlank(row)) {
                    list << helper.csvDataAsList(data: row)
                }
            }
            info = helper.convertRowListToMapList(list)
        }
        if (gint.verbose || (parameters.verbose == true)) {
            helper.log('info', info)
        }
        return info
    }
}
