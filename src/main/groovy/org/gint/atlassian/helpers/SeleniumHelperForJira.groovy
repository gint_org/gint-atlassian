/* Copyright (c) 2009, 2019 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.plugin.Gint


/**
 * Extend SeleniumHelper for Confluence specific methods. Otherwise use just like seleniumHelper.
 */
class SeleniumHelperForJira extends SeleniumHelperForAtlassian {

    SeleniumHelperForJira(final Gint gint) {
        super(gint)
    }

    /**
     * Get the default application name.
     * @return application name
     */
    @Override
    public String getDefaultApplication() {
        return 'jira'
    }
}
