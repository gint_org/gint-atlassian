/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.plugin.Gint

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

/**
 * Gint extensions for Slack
 */
@CompileStatic
@TypeChecked
class SlackHelper extends AtlassianHelper {

    /**
     * Constructor
     */
    SlackHelper(final Gint gint) {
        super(gint, 'slack')
    }

    public Map getServerInfoWithVerify(final Map parameters = null) {
        def info = getServerInfo(parameters?.cli)  // needed for version check later
        if (info == null) {
            gint.addFailedMessage("Token is not valid or server is not available.${parameters?.cli != null ? ' Cli: ' + parameters.cli : ''}")
            gint.stopNow = true
        }
        return info
    }

    /**
     * Contact server and retrieve information.
     * Be as generic as possible on output scanning to handle CLI changes - ignore case and spacing.
     * <pre>
     * </pre>
     * @return Map with information.
     */
    public Map<String, Object> getServerInfo(final inCli = null) {
        return getServerInfo(inCli instanceof Map ? inCli : [cli: inCli])
    }

    @Override
    public Map<String, Object> getServerInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {
            def cmd = cli.trim() + ' --action getServerInfo --outputFormat 2 --dateFormat "' + helper.getJsonDateFormatString() + '"'

            def p = gint.getParallelHelper().shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()

            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]
                info.cli = cli
                int index = 0

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing


                info.product           = helper.getPrettyFormatValue(string, 'product')
                info.url               = helper.getPrettyFormatValue(string, 'base url') // different than others
                info.user              = helper.getPrettyFormatValue(string, 'user')
                info.userId            = helper.getPrettyFormatValue(string, 'user id')
                info.team              = helper.getPrettyFormatValue(string, 'team')
                info.teamId            = helper.getPrettyFormatValue(string, 'team id')
                info.license           = helper.getPrettyFormatValue(string, 'license information')
                info.client            = 'slack'

                helper.removeNullValues(info)

                if (info.product == null) {
                    p.printOutList()
                    addMessage 'warning', 'Could not determine server information from the data returned from the request.'
                }

            } else if (gint.getVerbose()) {
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', "Configuration missing: cli and slackCli properties are blank or not defined."
        }
        if (gint.getVerbose()) {
            if (info) {
                helper.log('serverInfo', info)
            } else {
                helper.log('cli', cli)
            }
        }
        return info
    }

    /**
     * Look for an id in data, looks for pattern like (12345) or id: 12345 or id 12345 or id=12345 or . : 12345
     * @param data - data to be searched
     * @param occurrance - 1 based, 1 is first occurrence (default), 2 is second occurrence
     * @return id or null
     */
    static public String searchForSlackId(final data, final int occurrence = 1) {
        def matcher = (data.toString() =~ /(?:id ([A-Z][A-Z0-9]+))/)
        for (int i = 1; i < occurrence && matcher.find(); i++) { // skip earlier occurrances
        }
        return (matcher.find() ? matcher.group(1) : null)
    }
}
