/*
 * Copyright (c) 2009, 2018 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.helpers.SeleniumHelper
import org.gint.plugin.Gint

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * GINT extensions for Atlassian products.
 * Provides functions to support easier testing for a number of Atlassian and related products.
 * - Atlassian CLI - https://bobswift.atlassian.net/browse/ACLI
 * - Jira
 * - Confluence
 * - Bitbucket
 * - Bamboo
 * - Slack
 * - ...
 */
@CompileStatic
@TypeChecked
class AtlassianHelper extends JsapHelper {

    static protected Set<String> applications = ['confluence', 'jira']as Set // used by Selenium helper for Atlassian

    /**
     * Constructor
     */
    AtlassianHelper(final Gint gint, final String name = null) {
        super(gint, name ?: 'cli')
        if (gint.tearDownStrategy == null) {
            gint.setTearDownStrategy('before') // Good default to leave constructed entities available for debug and manual testing
        }
    }

    /**
     * Get list of applications that are supported for login and related
     * @return application list
     */
    public Set<String> getApplications() {
        return applications
    }

    /**
     * Check for server configuration and availability.
     * @param cli - cli to use, otherwise it will default to standard cli lookup
     * @param log - true to log information about the server
     * @return true if cli is configured and server is available and set parameters.info to the server info
     */
    public boolean isServerAvailable(final Map parameters = null) {
        def info = getServerInfo(parameters?.cli)  // use to verify cli is configured and server is available
        def result = (info != null)
        if (result && (parameters?.log == true)) {
            helper.log('serverInfo', info)
            helper.log('cli', parameters?.cli)
        }
        return result
    }

    /**
     * Sub class should implement if valid server information can be retrieved
     * @param cli - cli to use, defaults to
     * @return Map with information based on sub class or null
     */
    public Map getServerInfo(final cli = null) {
        return null  // sub class must implement if valid server information can be retrieved
    }

    /**
     * Sub class can override this if necessary
     */
    public Map getServerInfo(final Map parameters) {
        return [cli: parameters?.cli ?: getCli()] // no details available
    }

    /**
     * Get server info with verification handling - similar to getServerInfo
     * If server not available do standard things like add failure message, log information, and set stop on fail
     * @return same as getServerInfo
     */
    public Map getServerInfoWithVerify(final cli) {
        return getServerInfoWithVerify([cli: cli])
    }

    public Map getServerInfoWithVerify(final Map<String, Object> parameters = null) {
        Map info = getServerInfo(parameters)  // needed for version check later
        //helper.log 'info', info

        if (info == null) {
            gint.addFailedMessage("Server is not available${parameters?.cli == null ? '.' : ' with cli ' + helper.quoteString(parameters.cli)}.")
            gint.stopNow = true
        } else if ((parameters?.minVersion != null) //
        && (parameters.minVersion instanceof Integer) //
        && (parameters.version instanceof Integer) //
        && (((Integer)parameters.minVersion) > ((Integer) info.version))) {
            gint.addFailedMessage("Server version ${info.version} is less than the required version ${parameters.minVersion}.")
            gint.stopNow = true
        }
        return info
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    public getSeleniumHelper() {

        SeleniumHelper newHelper

        if (this instanceof JiraHelper) {
            newHelper = new SeleniumHelperForJira(gint)
        } else if (this instanceof ConfluenceHelper) {
            newHelper = new SeleniumHelperForConfluence(gint)
        } else {
            newHelper = new SeleniumHelperForAtlassian(gint)
        }
        gint.setSeleniumHelperIfNotDefined(newHelper)

        //helper.log 'selenium helper set to', gint.@seleniumHelper
        return gint.seleniumHelper
    }
}
