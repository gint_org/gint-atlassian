/* Copyright (c) 2009, 2019 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.helpers.SeleniumHelper
import org.gint.plugin.Gint
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Extend SeleniumHelper for Atlassian specific methods. Otherwise use just like seleniumHelper.
 */
@CompileStatic
@TypeChecked
class SeleniumHelperForAtlassian extends SeleniumHelper {

    static protected String defaultApplication = 'confluence'

    protected Map<String, Object> serverInfo

    static String mpDownloadsUrl = 'https://marketplace.atlassian.com/download/plugins'  // needs key and optionally version
    static String upmUrl = '/plugins/servlet/upm'

    // Login paths for atlassian products. Used in seleniumHelper at least.
    // It was convenient to locate them all together here instead of splitting them out to each subclass
    static protected Map loginPaths = [
        confluence: '/login.action',
        jira:       '/login.jsp',
        bamboo:     '/userlogin!default.action',
        bitbucket:  '/login',
    ]

    static protected Map adminLoginPaths = [
        confluence: '/admin/console.action',
        jira:       '/secure/admin/projectcategories/ViewProjectCategories!default.jspa', // base does not force admin login at least on 6.3
        bamboo:     '/admin/administer.action',
        bitbucket:  '/admin',
    ]

    static protected Map loginDefaultResultTitleText = [
        confluence: ~/(?i).*((Confluence)|(Dashboard)).*/, // Differences on default behavior going to jira or confluence
        jira:       'Dashboard',
        bamboo:     'Dashboard',
        bitbucket:  'Projects'
    ]
    static protected Map adminLoginDefaultResultTitleText = [
        confluence: 'Administration',
        jira:       ~/(?i).*Categories.*/,
        bamboo:     'Bamboo Administration',
        bitbucket:  'Administration',
    ]

    // Handle these potential responses from UPM - there may be more we don't know about
    static protected List<LinkedHashMap> upmErrorList = [
        [findText: 'An error occurred', message: 'Install failed. You will need to go to the server logs to determine the cause.'],
        [findText: 'cannot be enabled', message: 'Plugin could not be enabled. You will need to go to the server logs to determine the cause.'],
        [findText: 'Problem accessing', message: 'Plugin reference is invalid. Perhaps the reference is not a plugin.'],
    ]

    static protected Map mpKeySpecialValues = [
        upm:                 'com.atlassian.upm.plugin-license-storage-plugin',
        questions:           'com.atlassian.confluence.plugins.confluence-questions',
        calendars:           'com.atlassian.confluence.extra.team-calendars',
        'source-editor':     'com.atlassian.confluence.plugins.editor.confluence-source-editor',
        'jira-calendars':    'com.atlassian.jira.ext.calendar',
        agile:               'com.pyxis.greenhopper.jira',
        'service-desk':      'com.atlassian.servicedesk',
        'rest-browser':      'com.atlassian.labs.rest-api-browser',
        all: { Map parameters ->
            def newSet = []as Set
            parameters.aliasMap?.each { key, value ->
                if (isValidLookingMpKey(value)) {
                    newSet << value  // add all the mpKeys from the aliasMap
                }
            }
            return newSet
        },
    ]

    /**
     *  Extension of selenium helper in a atlassian context
     */
    SeleniumHelperForAtlassian(final Gint gint) {
        super(gint)
    }

    public void setServerInfo(Map<String, Object> serverInfo) {
        this.serverInfo = serverInfo
    }

    public Map<String, Object> getServerInfo() {
        return serverInfo
    }

    /**
     * Get list of applications that are supported for login and related
     * @return
     */
    public Set<String> getApplications() {
        return loginPaths.keySet()
    }

    public Map<String, String> getLoginPaths() {
        return loginPaths
    }
    public Map<String, String> getAdminLoginPaths() {
        return adminLoginPaths
    }

    /**
     * Find a suitable application based on the string values provided looking for the application name
     * @param list of names that might indicate the application
     * @return application found or null
     */
    public String findApplication(Collection<String> list) {
        def application = null
        if (list != null) {
            for (def string : list) {
                for (def key : getApplications()) {
                    if (string?.contains(key)) {
                        application = key
                        break
                    }
                }
                if (application != null) {
                    break
                }
            }
        }
        return application
    }

    /**
     * Get the default application name. Subclass should set correctly
     * @return
     */
    public String getDefaultApplication() {
        return defaultApplication
    }

    public void setDefaultApplication(application) {
        defaultApplication = application
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return partial url
     */
    @Override
    public getLoginPath(Map parameters = null) {
        return loginPaths[parameters?.application ?: getDefaultApplication()]
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return partial url
     */
    @Override
    public getAdminLoginPath(Map parameters = null) {
        return adminLoginPaths[parameters?.application ?: getDefaultApplication()] // default to Confluence
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return text to find on result page
     */
    @Override
    public getLoginDefaultResultTitleText(Map parameters = null) {
        return loginDefaultResultTitleText[parameters?.application ?: getDefaultApplication()]
    }

    /**
     * Product specific login path needed to be added to base url - implement in product specific like SeleniumHelperForConfluence
     * @param parameters - not really needed, but may become important when the result is release dependent
     * @return text to find on result page
     */
    @Override
    public getAdminLoginDefaultResultTitleText(Map parameters = null) {
        return adminLoginDefaultResultTitleText[parameters.application ?: getDefaultApplication()]
    }

    /**
     * If it looks like a controlled admin access has been enabled on this instance, then do the login
     * @param parameters - really only need the password for this
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public void handleAdminAccess(Map parameters = null) {

        if (driver.getTitle().toLowerCase().contains('administrator access')) {
            // look for password input field in the most general way (needed at least for JIRA)
            def passwordElement = null
            def inputElements = findElements(tag: 'input')
            assert inputElements?.size() > 1 // must be at least 2 input fields on the screen to login

            inputElements.each { element ->
                def attributes = helper.getWebElementAttributes(element)
                if (attributes.type != 'hidden') {
                    if ((attributes.name?.toLowerCase()?.contains('password') || attributes.id?.toLowerCase()?.contains('password'))) {
                        passwordElement = element
                    }
                }
            }
            assert passwordElement  // find password input field
            passwordElement.clear()
            passwordElement.sendKeys(parameters?.password ?: '') // password needs to be provided
            passwordElement.submit() // submit form
            delay()  // show the result screen if delay is specified
        } else { //if (gint.verbose) {
            helper.log('title', driver.getTitle())
            message 'info', 'No controlled admin access screen found.'
        }
    }

    /**
     * Calculate and validate parameters needed for installPlugin
     * @param plugin: global.plugin, // optional - file name or url path, if not provided a plugin will be searched for in the directory search
     * @param mpKey: global.mpKey,   // optional - marketplace key, if provided, plugin is ignored
     * @param version: global.version, // marketplace version
     * @param directoryList: global.directoryList, // places to look for an obr or jar file
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public Map<String, String> getValidatedInstallParameters(final Map inParameters) {

        assert inParameters // parameters must be provided
        Map parameters = inParameters.clone() // values will be updated
        String errorMessage = null
        boolean gintAvailable = (binding.gint instanceof Gint) // almost always should be case, not tested otherwise

        if (!parameters.server.startsWith('http')) {
            errorMessage = 'Server parameter is not valid. It does not start with http.'

        } else if ((parameters.mpKey != null) && (parameters.mpKeySet == null)) {

            if (parameters.aliasMap == null) { // not already provided
                parameters.aliasMap = helper.getMatchingProperties(namePattern: parameters.aliasNamePattern, valuePattern: parameters.aliasValuePattern)
            }

            parameters.mpKeySet = getValidMpKeys(parameters) // validated keys only

            if (parameters.mpKeySet.size() == 0) {
                errorMessage = 'No valid mpKeys were provided'
            } else {
                parameters.pluginIsUrl = true
                parameters.plugin = null // if mpKey is provided the plugin will be ignored, setting to null helps document this
            }

        } else {
            if (parameters.plugin == null) {
                List<File> fileList = []
                if (parameters.directoryList == null) { // if not directly provided
                    // use directory parameter or search target first, then current directory
                    parameters.directoryList = parameters.directory ? [parameters.directory]: [
                        gintAvailable ? gint.getTargetDirectory() : null,
                        '.'
                    ]
                }
                parameters.directoryList?.each { String directoryName ->
                    if (directoryName != null) {
                        [
                            /.*\.obr/,
                            /.*(?<!sources)\.jar/
                        ].each { regex ->
                            // favor an obr over a jar, never a sources jar!
                            fileList += helper.getFileList(directoryName, regex)
                        }
                    }
                }
                if (fileList.size() == 0) {
                    helper.log('directoryList', parameters.directoryList)
                    errorMessage = 'Plugin file or url is required if it cannot be found in the directory list. For example: -Dplugin=target/bobswift-cache-6.5.0-obr or -Dplugin=http://mini.local:8300/artifact/CONF-CACHE/shared/build-155/target/bobswift-cache-6.5.0.obr'
                } else {
                    parameters.plugin = fileList[0].getAbsolutePath()
                }
            } else { // verify plugin exists and get the absolute path to it
                parameters.pluginIsUrl = parameters.plugin?.contains("://")
                if (!parameters.pluginIsUrl) {
                    def file = new File(parameters.plugin)
                    if (file.exists()) {
                        parameters.plugin = file.getAbsolutePath()
                    } else {
                        errorMessage = 'Could not find plugin: ' + parameters.plugin
                    }
                }
            }
        }
        if (errorMessage != null) {
            if (gintAvailable) {
                binding.gint.addTestFailedMessage(errorMessage)
            } else {
                message 'error', errorMessage
                assert false // see error Message
            }
        }
        return parameters
    }

    /**
     * Expand out mpKeys to valid looking keys (have a period)
     * @param parameters
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected Set<String> getValidMpKeys(Map parameters) {
        Set<String> newSet = []as Set
        helper.csvDataAsList(data: parameters.mpKey).each { mpKey ->
            mpKey = mpKey?.trim()
            helper.dLog('mpKey', mpKey)
            if (!helper.isBlank(mpKey)) { // not blank
                if (parameters.aliasMap?.keySet()?.contains(mpKey)) {
                    mpKey = parameters.aliasMap[mpKey] // apply map if found
                }
                if (mpKey.contains(',')) { // recursive comma separated list
                    newSet += getValidMpKeys(parameters + [mpKey: mpKey]) // expand out another level deeper
                } else if (isValidLookingMpKey(mpKey)) { // looks like a valid entry
                    newSet << mpKey
                } else {
                    def mpKeyLower = mpKey.toLowerCase()
                    if (mpKeySpecialValues.keySet().contains(mpKeyLower)) {
                        def value = mpKeySpecialValues[mpKeyLower]
                        if (value instanceof Closure) {
                            newSet += value.call(parameters)  // closure should produce a list of keys to add
                        } else {
                            newSet << value
                        }
                    } else {
                        message 'warning', 'Ignore invalid looking mpKey: ' + mpKey
                    }
                }
                helper.log('mpKey', mpKey)
                helper.log('newSet', newSet)
            }
        }
        return newSet
    }

    static public boolean isValidLookingMpKey(value) {
        return value.toString().contains('.') && !value.toString().contains(',')
    }

    /**
     * Install a plugin. Parameters must be validated first using getValidatedInstallParameters. See above.
     * @return true but can also exit on assert errors
     */
    public boolean installPlugin(final Map parameters) {
        assert parameters // parameters must be provided
        boolean result = true
        if (parameters.mpKeySet != null) {
            parameters.mpKeySet.each { mpKey ->
                parameters.plugin = getPluginFromMpKey(parameters)
                result = result && installPluginInternal(parameters)
                if (!result && (parameters.continueOnError == false)) {
                    return false
                }
            }
        } else {
            if (parameters.mpKey != null) {
                parameters.plugin = getPluginFromMpKey(parameters)
            }
            result = installPluginInternal(parameters)
        }
        return result
    }

    protected String getPluginFromMpKey(Map parameters) {
        return "${mpDownloadsUrl}/${parameters.mpKey}" + ((parameters.version != null) ? "/version/${parameters.version}" : '')
    }

    /**
     * Install a plugin. Parameters must be validated first using getValidatedInstallParameters.
     * @return true but can also exit on assert errors
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected boolean installPluginInternal(final Map parameters) {

        gotoUrl(url: "${parameters.server}${upmUrl}")

        //helper.log('parameters', parameters)
        def element = findElement(id: 'upm-upload', visible: true)
        assert element // upload found
        element.click()
        delay()
        def fieldId = 'upm-upload-' + (parameters.pluginIsUrl ? 'url' : 'file')
        element = findElement(id: fieldId, visible: true)
        assert element // looking for upm-upload-... field

        message 'info', 'Sending keys: ' + parameters.plugin + ', to field: ' + fieldId
        element.sendKeys(parameters.plugin)

        def elementList = findElements(text: 'Upload', visible: true, wait: 20)
        //helper.log('elements', elements)

        // There are multiple upload elements, find the upload button
        def upload = null
        for (e in elementList) {
            def attributes = helper.getWebElementAttributes(e)
            if ((attributes.type == 'submit') && (attributes.name = 'Upload')) {
                upload = e
                break
            }
        }
        assert upload // this is the upload button
        upload.click()
        delay()

        boolean errorFound = false
        def loop = parameters.pollingCount ?: 60 // times to poll - some systems accessed are VERY slow at installing
        def pollingInterval = 5  // seconds between polls
        for (int i = 1; i <= loop; i++) {
            element = findElement(text: 'Installed', visible: true, wait: pollingInterval, quiet: true)
            if (element != null) {
                message 'info', 'Found message with words: Installed'
                break
            }

            element = findElement(text: 'has been installed', visible: true, wait: pollingInterval, quiet: true)
            if (element != null) {
                message 'info', 'Found dialog with words: has been installed'
                break
            }

            // Let this error go as well. It really is just a warning
            element = findElement(text: 'This can indicate a problem with the base URL', visible: true, wait: pollingInterval, quiet: true)
            if (element != null) {
                break
            }

            // look for error indicators
            upmErrorList.each { errorMap ->
                element = findElement(text: errorMap.findText, quiet: true)
                if (element != null) {
                    message 'error', errorMap.message
                    errorFound = true
                }
            }
            if (errorFound) {
                break
            }
            message 'info', 'Still looking for a success indicator ... '
        }
        if (gint.verbose) {
            screenshot()
        }
        if (!errorFound && (element == null)) {
            screenshot(name: 'install_timeout.png')
            message 'error', 'Timeout looking for a successful install. Could be a slow system or an error that is not recognized (please report the details). '
        }
        return (!errorFound && (element != null))
    }

    /**
     * Open up add-on details in UPM.
     * @param mpKey - marketplace add-on key
     * @param server - base server url, if provided, it will go to the manage add-on screen, otherwise assumes you are already on manage add-on
     * @return - element representing expanded add-on on manage add=on screen
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected WebElement upmOpenAddOn(Map parameters) {
        assert parameters.mpKey  // mpKey is required
        if (parameters.server != null) {
            gotoUrl(url: parameters.server + '/plugins/servlet/upm#manage')
            delay()
            assert isMatchOnTitle(text: 'Manage add-ons', visible: true) // make sure we are on the right page
        }

        def elements = findElements(className: 'upm-plugin-key', present: true, attribute: 'value', attributeValue: parameters.mpKey)
        helper.dLog('elements', elements)
        helper.vLog('mpKey', parameters.mpKey)
        assert elements.size() > 0 // did we find the add-on?

        def element = elements[0] // use the first one found

        element = getParent(element)
        helper.dLog('element', element)

        element.click() // highlight the add-on
        delay()

        def expander = findElement(base: element, className: 'expander', visible: true)
        assert expander  // found expander to click

        expander.click()
        delay()
        return element
    }

    /**
     * Apply a standard Atlassian add-on license
     * @param element - represents expanded add-on entry, normally returned from upmOpenAddOn
     * @param license - license text, use blank to unset a license
     * @return true if license was applied correctly, false if it looks like there was an error
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected boolean upmApplyLicense(Map parameters) {
        def result = true
        assert parameters.license != null // license must be provided
        assert parameters.element || parameters.server  // add-on element or server must be provided
        def element = parameters.element

        if ((element == null) && (parameters.server != null)) {
            element = upmOpenAddOn(server: parameters.server, mpKey: parameters.mpKey)
        }

        def elements = findElement(base: element, xxxclassName: 'upm-plugin-license-edit', visible: true, wait: 10)
        helper.log('elements', elements)

        // add-on should now be expanded
        def edit = findElement(base: element, className: 'upm-plugin-license-edit', visible: true, wait: 10)
        if (edit) {
            edit.click() // to open up edit field if not already open
        } else {
            message 'info', 'Edit plugin license link not found. It license could be open already'
        }
        def licenseTextarea = findElement(base: element, tag: 'textarea', visible: true, wait: 10)
        assert licenseTextarea // found license entry

        helper.dLog('licenseTextarea', licenseTextarea)

        def currentLicense = helper.getWebElementAttributes(licenseTextarea).value.trim().replace('\n','')
        def futureLicense = parameters.license?.trim().replace('\n','')

        if (futureLicense != currentLicense) {  // something would change by applying new license
            licenseTextarea.clear()
            licenseTextarea.sendKeys(futureLicense)
            licenseTextarea.submit()
            // delay() don't do a delay here since the result message only shows for a brief period of time

            def resultMessage = findElement(base: element, className: 'upm-message-text', visible: true)
            assert resultMessage
            delay()
            helper.dLog('result message', resultMessage)
            def text = helper.getWebElementAttributes(resultMessage).text
            message 'result', text
            result = !text.contains('invalid') && !text.contains('cannot complete')
        } else {
            message 'info', 'Requested license is already set'
        }

        def expander = findElement(base: element, className: 'expander', visible: true)
        assert expander  // found expander to click

        expander.click()
        delay()

        return result
    }
}
