/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.atlassian.tasks.GintConfluenceConnectMacroTask
import org.gint.helpers.Helper
import org.gint.helpers.HtmlHelper
import org.gint.interfaces.CmdGenerator
import org.gint.plugin.Constants
import org.gint.plugin.Gint
import org.gint.tasks.GintTask
import org.gradle.api.Task

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Class to provide GINT command generator support.
 * A GINT command generator closure is a closure that takes a gint task as a parameter and produces a command string
 * suitable for running on the current platform.
 * This class helps manage the command generators by associating them with names and parameters used to construct them.
 */
@CompileStatic
@TypeChecked
class CmdGeneratorHelper extends org.gint.helpers.CmdGeneratorHelper {

    public CmdGeneratorHelper(final Gint gint) {
        super(gint)
    }

    public CmdGenerator getCmdGenerator(final String name, final Map parameters = null) {

        CmdGenerator cmdGenerator = super.getCmdGenerator(name, parameters)

        if (cmdGenerator == null) {
            switch (name) {
                case 'cli':
                case 'atlassian':
                case 'acli':
                    return getCmdGeneratorForAtlassian(parameters)

                case 'confluence':
                case 'confluencecloud':
                    return getCmdGeneratorForConfluence(parameters)

                case 'confluenceMacro':
                    return getCmdGeneratorForConfluenceMacro(parameters)

                case 'jira':
                case 'jiracloud':
                    return getCmdGeneratorForJira(parameters)

                case 'jsm':
                case 'jsm':
                    return getCmdGeneratorForJsm(parameters)

                case 'servicedesk': // deprecated
                case 'servicedeskcloud': // deprecated
                    return getCmdGeneratorForServiceDesk(parameters)

                case 'bamboo':
                    return getCmdGeneratorForBamboo(parameters)

                case 'bitbucket':
                    return getCmdGeneratorForBitbucket(parameters)

                case 'bitbucketcloud':
                    return getCmdGeneratorForBitbucketCloud(parameters)

                case 'slack':
                    return getCmdGeneratorForSlack(parameters)

                case 'jsap':
                    return getCmdGeneratorForJsap(parameters)

                default: break
            }
        }
        return cmdGenerator
    }

    /**
     * Closure that generates a command string for Atlassian CLI client. The closure takes a task definition
     * as a parameter. The task definition can have keys:
     * <ul>
     * <li> name - task name
     * <li> product - atlassian product
     * <li> action - CLI action to take, defaults to name
     * <li> file - file CLI parameter value, if true - defaults to output file based on task name
     * <li> parameters (or parameters2) - other CLI parameters
     * <ul>
     * Parameters:
     * - action
     * - file - file name or true to use standard output file name based on task name
     * - parameters
     * @param name of product
     * @return closure that generates cli command string for a task
     */
    public CmdGenerator getCmdGeneratorForAtlassian(final Map parameters) {

        def cli = helper.getWithExtendedLookup(parameters?.cli, 'atlassianCli', 'acli')
        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def action = map?.action ?: map?.name // default action to name of task
            def product = map?.product ?: (parameters?.product ?: '')
            def fileParameter = (map?.file ? helper.jsapParameter('file', ((map?.file == true) ? gint.getOutputFile(map?.name) : map?.file)) : '')
            def parameterString = getParameterStringForJsapGenerator(map)
            def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
            return "${cli} ${product} --action ${action} ${fileParameter} ${parameterString ?: ''} ${map?.parameters2 ?: ''} ${redirect}".toString()
        }
    }

    /**
     * Closure that generates a command string for Confluence CLI client. The closure takes a task definition
     * as a parameter. The task definition can have keys:
     * <ul>
     * <li> name - task name
     * <li> action - CLI action to take, defaults to name
     * <li> file - file CLI parameter value, if true - defaults to output file based on task name
     * <li> parameters (or parameters2) - other CLI parameters
     * <li> space - space name
     * <li> title - title name
     * </ul>
     * @return closure that generates cli command string for a task
     */
    public CmdGenerator getCmdGeneratorForConfluence(final Map parameters = null) {

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null
            return getCmdForConfluence(getCli('confluence', parameters), map)
        }
    }

    public String getCli(final String application, final Map parameters = null) {
        return helper.getWithExtendedLookup(parameters?.cli ?: helper.getParameterValue('cli'), application + 'Cli', 'acli ' + application)
    }

    public String getCmdForConfluence(final String cli, final Map map) {
        def action = map?.action ?: map?.name // default action to name of task
        def fileParameter = ((map?.file != null) ? helper.jsapParameter('file', ((map?.file == true) ? gint.getOutputFile(map?.name) : map?.file)) : '')
        def space = map?.space ? helper.jsapParameter('space', map?.space) : ''
        def title = map?.title ? helper.jsapParameter('title', map?.title) : ''
        def parameterString = getParameterStringForJsapGenerator(map)
        def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
        return "${cli} --action ${action} ${space} ${title} ${fileParameter} ${parameterString} ${map?.parameters2 ?: ''} ${redirect}".toString()
    }

    /**
     * Generate closure that will generate the commands for Confluence macro task
     * <p> The task definition can have keys:
     * <ul>
     * <li> macro - name of macro being tested
     * <li> render - referenced task to render, or new content page with *content* parameter, or just render page
     * <li> parameters - macro parameters
     * <li> body - macro body, null if and only if macro configured to not have a body
     * <li> inlineBody - true to make body inline with macro data for special cases where spacing is critical
     * <li> beforeText - text that goes directly before the macro being tested
     * <li> afterText - text that goes directly after the macro being tested
     * <li> expected - false if macro is expected to produce a standard Confluence macro error
     * <li> attachment - false to avoid automatic attachment handling, otherwise name of attachment if different from default
     * <li> compare - for file comparisons
     * <li> imageCompare - to generate image out of the macro output for image compares
     * <li> encoding - file encoding (charset) like UTF-8
     * </ul>
     * <p> The following task keys are modified as a result of this:
     * <ul>
     * <li> rendered - name of render output file, based on output directory and task name - ends with .html
     * <li> macroOutput - name of macro output file, based on output directory and task name - ends with .out
     * <li> output - output parameter is updated if not specified
     * <ul>
     * <li> file - defaults to the macro output file
     * <li> data - set to standard Confluence error if expected result is error
     * <li> failData - set to standard Confluence error if expected result is success
     * </ul>
     * <li> expected
     * <li> attachmentFile
     * <li> renderedMacroFile
     * <li> standardInput - set to confluence CLI commands to generate page and content
     * </ul>
     * @result closure that generates a set of commands given a task
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    public CmdGenerator getCmdGeneratorForConfluenceMacro(final Map parameters = null) {

        def cli = getCli('confluence', parameters)
        return (CmdGenerator) { Task t ->

            Map map = getUserMap(t) // may be null
            //helper.log 'map', map

            // if not GintTask or not macro related, then just use the standard confluence cli task
            if (!(t instanceof GintTask) || ((map?.macro == null) && (map?.render == null))) {
                return getCmdGeneratorForConfluence(parameters).callback(t)

            } else { // standard confluence macro testing

                GintTask task = (GintTask) t

                def name = gint.getOutputFile(task.name, '') // no extension
                def rendered = name + '.html' // rendered output - entire page

                Task referencedTask = null
                Map referencedMap = null

                String render = map?.render?.toString() // render requested referencing another task
                if (helper.isNotBlank(render)) {
                    referencedTask = gint.project.tasks.getByName(render)
                    referencedMap = getUserMap(referencedTask)  // could be null
                    //helper.log 'referenced', referencedTask
                    //helper.log 'referenced map', referencedMap
                }

                def isReferenced = (referencedTask != null) && (referencedTask.name != map?.name)
                def isNewContent = helper.isNotBlank(render) && !isReferenced && (map?.content != null)
                def updatePage = map?.macro || isNewContent
                def isStorageFormat = (map?.wiki != true) && ((map?.storageFormat == true) || ((parameters?.storageFormat == true) && (map?.storageFormat != false)))

                if (referencedMap == null) {
                    referencedMap = map // convenient for code below
                }
                if ((map?.output instanceof Map) && (map?.output.'file' == null)) {  // if user is doing some analysis, default the name
                    map?.output.'file' = rendered
                }
                def encoding = null

                // check for errors in failData if success is expected, or in data if an error is being looked for

                def errorIndicatorLocation = task.isExpectedResult(0, map?.expected, map?.name) ? 'failData' : 'data'
                map[errorIndicatorLocation] = helper.getAsCollection(map[errorIndicatorLocation])

                // TODO: this should be checked and tested as an edge case
                if (parameters?.macroErrorIndicator && (map?.render == null)) {  // no default macro error indicator for just rendering data
                    // hack to provide user some way to avoid including the default cmd generator error indicator
                    // using: failData: [null] will avoid it.
                    if (!(map[errorIndicatorLocation] instanceof List) || !map[errorIndicatorLocation]?.contains(null)) {
                        map[errorIndicatorLocation] = map[errorIndicatorLocation] + [
                            parameters.macroErrorIndicator
                        ]
                    }
                }
                //helper.log 'map', map

                // must reset now to prevent CLI command error checking kicking in
                task.expected = (map?.cliExpected != null) ? task.convertExpectedToInteger(map?.cliExpected) : 0
                encoding = map?.encoding ?: parameters?.encoding ?: task.encoding

                def macroMap = (map?.macro != null) ? map : referencedMap
                //helper.log 'macroMap', macroMap
                //helper.log 'map', map

                def content = (macroMap?.content ?: generateMacroPageContent(macroMap, parameters, isStorageFormat)).replace('\n', ' \\n ')

                // Common
                def builder = new StringBuilder()
                builder.append(' --space ')
                builder.append(helper.doubleQuoteString(referencedMap?.space ?: parameters?.space ?: gint.getConfluenceHelper().getSpaceKey()))
                builder.append(' --title ')
                builder.append(helper.doubleQuoteString(referencedMap?.title ?: referencedMap?.name))
                if (encoding != null) {
                    builder.append(' --encoding ').append(encoding)
                }

                def common = builder.toString()

                List<String> actionList = new ArrayList<String>()

                if (updatePage) {  // store page action
                    actionList << helper.jsapParameter([
                        action: 'storePage',
                        parent: map?.parent ?: parameters?.parent ?: gint.getConfluenceHelper().getParent(),
                        content: content,
                    ] + (isStorageFormat ? [noConvert: null] : [:]), '"') + (map?.storeParameters ?: '') + common
                }

                // TODO: Need to document this as a performance improvement
                // it would be better if we default to skip if there is no data comparison or just use the macro render support ???
                if (map.skipRender == true) {
                    gint.project.ant.delete(file: rendered, quiet: true) // make sure there is no old data
                } else {
                    actionList << '-a renderPage -f ' + gint.helper.doubleQuoteString(rendered) + common
                }

                // TODO: Not sure we want to support CLI cookie parameters any more here
                //if (map?.cookies != null) {
                //    return map?.cookies != null ? [cookies: map?.cookies] : []
                //}

                // Render just the macro in the context of the page (either macro page or referenced page)
                // Don't need the info if Cloud, we have to get the data in another way
                if (!(task instanceof GintConfluenceConnectMacroTask)) {
                    // Get macro content if request is for a macro
                    if (((map?.macro != null) || (isReferenced && (referencedMap?.macro != null)))) {
                        map.renderedMacroFile = gint.getOutputFile(map?.name, '.txt')
                        gint.project.ant.delete(file: map?.renderedMacroFile, quiet: true) // make sure there is no old data
                        // render the macro content
                        actionList << helper.jsapParameter([
                            action: 'render',
                            file: map?.renderedMacroFile,
                            content: getMacroContent(macroMap, parameters, isStorageFormat).replace('\n', ' \\n '),
                            clean: null,
                        ] + (isStorageFormat ? [noConvert: null] : [:]) + (map?.cookies != null ? [cookies: map?.cookies] : [:]), '"') + common
                    }
                }

                // Option to retrieve an attachment associated with the page
                if ((map?.attachment != null) && (map.attachment != false)) {
                    def attachmentName = ((map.attachment instanceof String) || (map.attachment instanceof GString)) ? map.attachment : (macroMap.attachment ?: macroMap.name)
                    if (!helper.isNotBlank(map.attachmentFile)) {
                        map.attachmentFile = gint.getOutputFile(attachmentName, '.txt')
                    }
                    // get attachment action
                    actionList << helper.jsapParameter([
                        action: 'getAttachment',
                        file: map.attachmentFile,
                        name: attachmentName,
                    ], '"') + common
                }
                //helper.log 'action list', actionList

                builder = new StringBuilder()
                builder.append(cli).append(' -a run ').append(!gint.getVerbose() ? ' --quiet' : '')
                //helper.log 'action', build.toString()

                task.standardInput = (actionList.collect { it }).join(Constants.EOL)

                // This is an alternative to using standard input that it a bit nicer for debugging
                // but does have disadvantage of having to quote the command lines which may introduce quoting problems in rare cases
                //actionList.each {
                //    builder.append(' -i ').append(helper.quoteString(it))
                //}

                return builder.toString()
            }
        }
    }

    /**
     * Generate standard page content for a Confluence macro test - wiki format
     * @param task
     * @param file to write page content to
     */
    public String generateMacroPageContent(final Map map, final Map parameters, final boolean isStorageFormat) {

        def content = new StringBuilder()
        content.append('\n')

        content.append((map?.macroPageBegin != null) ? map?.macroPageBegin : (isStorageFormat ? parameters?.macroStoragePageBegin : parameters?.macroPageBegin) ?: '')

        if (map?.omitDocumentation != true) {
            content.append isStorageFormat ? '<h4>Test</h4>' : '\n\nh4. Test'
            content.append('\n')
        }

        if ((map?.wiki == true) || ((parameters?.wiki == true) && (map?.wiki != false))) {  // legacy wiki support
            content.append "\n{wiki}"
        }

        content.append getMacroContent(map, parameters, isStorageFormat)

        if ((map?.wiki == true) || ((parameters?.wiki == true) && (map?.wiki != false))) {
            content.append "\n{wiki}"
        }

        // Add documentation section unless specifically requested to omit - usually because noformat is used in body
        if (map?.omitDocumentation != true) {
            if (isStorageFormat) {  // page is going to be saved in storage format - yuk! Have to convert from wiki to storage format
                // Example: <ac:macro ac:name="unmigrated-inline-wiki-markup"><ac:plain-text-body><![CDATA[{wiki} {info} xxxxx {info} {wiki}]]></ac:plain-text-body></ac:macro>
                content.append generateMacroStorageFormat(
                                macro: 'unmigrated-inline-wiki-markup',
                                //parameters: ['atlassian-macro-output-type': 'BLOCK'],
                                body: HtmlHelper.cdataWrapper(getMacroPageContentHeadSection(map, parameters, isStorageFormat)),
                                bodyType: 'plain-text-body',
                                inlineBody: true,
                                )
            } else {
                content.append getMacroPageContentHeadSection(map, parameters, isStorageFormat)
            }
        }

        content.append('\n')
        content.append((map?.macroPageEnd != null) ? map?.macroPageBegin : (isStorageFormat ? parameters?.macroStoragePageEnd : parameters?.macroPageEnd) ?: '')
        return content.toString()
    }

    /**
     * Documentation section of the macro page content
     * @param task
     * @param parameters
     * @return wiki markup of the page content
     */
    protected getMacroPageContentHeadSection(final Map map, final Map parameters, final boolean isStorageFormat = false) {
        def content = new StringBuilder()

        content.append "\n\nh4. Documentation"
        content.append('\n')
        content.append "\n || Macro | ${map?.macro} |"
        if (map?.description) {
            content.append "\n || Description | ${map?.description} |"
        }
        content.append '\n || Details | {noformat:nopanel=true}'
        content.append getMacroContent(map, parameters, isStorageFormat, false)
        if (isStorageFormat) {
            content.append('\n')
            def macro = [
                name: map?.macro,
                parameters: map?.parameters,
                inlineBody: map?.inlineBody,
                bodyType: map?.bodyType,
                body: map?.body,
            ]
            def helper2 = new Helper(new Gint())
            helper2.setMessageClosure(Helper.getStandardMessageClosure(content))
            helper2.log('macro', Helper.removeNulls(macro))  // provide a bit easier to read information
        }
        content.append " {noformat} |"

        if (map?.beforeText) {
            content.append "\n || Before text | {noformat:nopanel=true} ${map?.beforeText} {noformat} |"
        }
        if (map?.afterText) {
            content.append "\n || After text | {noformat:nopanel=true} ${map?.afterText} {noformat} |"
        }
        return content.toString()
    }

    /**
     * Get macro content including before and after text respecting content type of wiki (default) or storage format
     * @param task
     * @param parameters
     * @return macro string
     */
    protected String getMacroContent(final Map map, final Map parameters, final boolean isStorageFormat, final boolean includeBeforeAfter = true) {
        return isStorageFormat ? getStorageFormatMacroContent(map, parameters, includeBeforeAfter) : getWikiMacroContent(map, parameters, includeBeforeAfter)
    }

    /**
     * Get the wiki macro content including before and after text.
     * @param task
     * @param parameters
     * @return macro string like: beforeText{macro:parameters}body{macro}afterText
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected String getWikiMacroContent(final Map map, final Map parameters, final includeBeforeAfter = true) {
        def builder = new StringBuilder()
        builder.append('\n')
        if (includeBeforeAfter && (map?.beforeText != null)) {
            builder.append(map?.beforeText)
        }
        builder.append('{').append(map?.macro)
        if (map?.parameters != null) {
            builder.append(':')
            builder.append(map?.parameters instanceof Map ? helper.mapToSeparatedString(map?.parameters, '|', '=') : map?.parameters)
        }
        if (map?.parameters2 != null) {
            builder.append("|").append(map?.parameters2 instanceof Map ? helper.mapToSeparatedString(map?.parameters2, '|', '=') : map?.parameters2)
        }
        builder.append('}')
        if (map?.body != null) {
            def body = (map?.body instanceof Closure) ? map?.body.call() : map?.body
            def bodyBreak = (map?.inlineBody != true) ? '\n' : '' // whether to make body inline with macro data
            builder.append(bodyBreak).append(body).append(bodyBreak)
            builder.append('{').append(map?.macro).append('}')  // ending tag
        }
        if (includeBeforeAfter && (map?.afterText != null)) {
            builder.append(map?.afterText)
        }
        return builder.toString()
    }

    /**
     * Get the storage format macro content including before and after text. This is needed if the macro
     * does not support legacy wiki markup.
     * <pre>
     * Example:
     *     beforeText
     *     <ac:macro ac:name="info"><ac:parameter ac:name="title">xxxx</ac:parameter>
     *     <ac:rich-text-body><p>text</p></ac:rich-text-body>
     *     </ac:macro>
     *     afterText
     * </pre>
     * @param task
     * @param parameters
     * @return macro string like:
     */
    protected String getStorageFormatMacroContent(final Map map, final Map parameters, final includeBeforeAfter = true) {

        //helper.log 'storage map', map
        //helper.log 'storage parameters', parameters

        def builder = new StringBuilder()
        builder.append('\n')
        if (includeBeforeAfter && (map?.beforeText != null)) {
            builder.append(map?.beforeText)
        }

        builder.append(generateMacroStorageFormat(map))

        if (includeBeforeAfter && (map?.afterText != null)) {
            builder.append(map?.afterText)
        }
        return builder.toString()
    }

    /**
     * Given a map representing a macro definition, generate the storage format representation
     * <pre>
     * Map parameters
     *     macro - name
     *     parameters - macro parameters
     *     body - macro body
     *     inLineBody - not true will genenerate <p> wrapper
     *     bodyType - defaults to "rich-text-body"
     * </pre>
     * @param map
     * @return
     */
    public String generateMacroStorageFormat(final Map map) {

        def builder = new StringBuilder()
        builder.append('<ac:macro ac:name=\'').append(map?.macro).append('\'>')

        if (map?.parameters instanceof Map) {
            ((Map) map?.parameters).each { key, value ->
                builder.append('<ac:parameter ac:name=\'').append(key).append('\'>')
                builder.append(value ?: '')
                builder.append('</ac:parameter>')
            }
        } else if (map?.parameters != null) {
            builder.append(map?.parameters)  // assumed to be in storage format already !!!
        }

        if (map?.body != null) {
            def bodyType = map?.bodyType ?: 'rich-text-body'
            builder.append('<ac:').append(bodyType).append('>')
            builder.append((map?.inlineBody != true) ? '<p>' : '')
            builder.append(map?.body)
            builder.append((map?.inlineBody != true) ? '</p>' : '')
            builder.append('</ac:').append(bodyType).append('>')
        }
        builder.append('</ac:macro>')
        return builder.toString()
    }

    /**
     * Closure that generates a command string for JIRA CLI client. The closure takes a task definition
     * as a parameter. The task definition can have keys:
     * <ul>
     * <li> name - task name
     * <li> action - CLI action to take, defaults to name
     * <li> file - file CLI parameter value, if true - defaults to output file based on task name
     * <li> parameters (or parameters2) - other CLI parameters
     * <li> project - project name
     * <li> issue - issue name
     * </ul>
     * @return closure that generates cli command string for a task
     */
    public CmdGenerator getCmdGeneratorForJira(final Map parameters = null) {

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null
            return getCmdForJira(getCli('jira', parameters), map)
        }
    }

    public String getCmdForJira(final String cli, final Map map) {
        def action = map?.action ?: map?.name // default action to name of task
        def fileParameter = (map?.file ? helper.jsapParameter('file', ((map?.file == true) ? gint.getOutputFile(map?.name) : map?.file)) : '')
        def project = map?.project ? helper.jsapParameter('project', map?.project) : ''
        def issue = map?.issue ? helper.jsapParameter('issue', map?.issue) : ''
        def parameterString = getParameterStringForJsapGenerator(map)
        def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
        return "${cli} --action ${action} ${project} ${issue} ${fileParameter} ${parameterString} ${map?.parameters2 ?: ''} ${redirect}".toString()
    }

    public CmdGenerator getCmdGeneratorForJsm(final Map parameters = null) {

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null
            return getCmdForJsm(getCli('jsm', parameters), map)
        }
    }

    // servicedesk is now jsm (Jire service management) - leave in until Gint 4.0
    @Deprecated
    public CmdGenerator getCmdGeneratorForServiceDesk(final Map parameters = null) {

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null
            return getCmdForJsm(getCli('servicedesk', parameters), map)
        }
    }

    // almost the same as jira: issue -> request
    public String getCmdForJsm(final String cli, final Map map = null) {
        def action = map?.action ?: map?.name // default action to name of task
        def fileParameter = (map?.file ? helper.jsapParameter('file', ((map?.file == true) ? gint.getOutputFile(map?.name) : map?.file)) : '')
        def project = map?.project ? helper.jsapParameter('project', map?.project) : ''
        def request = map?.request ? helper.jsapParameter('request', map?.request) : ''
        def parameterString = getParameterStringForJsapGenerator(map)
        def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
        return "${cli} --action ${action} ${project} ${request} ${fileParameter} ${parameterString} ${map?.parameters2 ?: ''} ${redirect}".toString()
    }

    /**
     * Closure that generates a command string for Bamboo CLI client. The closure takes a task definition
     * as a parameter. The task definition can have keys:
     * <ul>
     * <li> name - task name
     * <li> action - CLI action to take, defaults to name
     * <li> file - file CLI parameter value, if true - defaults to output file based on task name
     * <li> parameters (or parameters2) - other CLI parameters
     * <li> project - project name
     * <li> build - build name
     * <li> job - job name
     * </ul>
     * @return closure that generates cli command string for a task
     */
    public CmdGenerator getCmdGeneratorForBamboo(final Map parameters = null) {

         return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null
            return getCmdForBamboo(getCli('bamboo', parameters), map)
        }
    }

    public String getCmdForBamboo(final String cli, final Map map) {
        def action = map?.action ?: map?.name // default action to name of task
        def fileParameter = (map?.file ? helper.jsapParameter('file', ((map?.file == true) ? gint.getOutputFile(map?.name) : map?.file)) : '')
        def project = map?.project ? helper.jsapParameter('project', map?.project) : ''
        def plan = map?.plan ? helper.jsapParameter('plan', map?.plan) : ''
        def build = map?.build ? helper.jsapParameter('build', map?.build) : ''
        def job = map?.job ? helper.jsapParameter('job', map?.job) : ''
        def number = map?.number ? helper.jsapParameter('number', map?.number) : ''
        def parameterString = getParameterStringForJsapGenerator(map)
        def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
        return "${cli} --action ${action} ${project} ${plan} ${build} ${job} ${number} ${fileParameter} ${parameterString} ${map?.parameters2 ?: ''} ${redirect}".toString()
    }

    /**
     * Closure that generates a command string for Stash CLI client. The closure takes a task definition
     * as a parameter. The task definition can have keys:
     * <ul>
     * <li> name - task name
     * <li> action - CLI action to take, defaults to name
     * <li> file - file CLI parameter value, if true - defaults to output file based on task name
     * <li> parameters (or parameters2) - other CLI parameters
     * <li> project - project name
     * </ul>
     * @return closure that generates cli command string for a task
     */
    public CmdGenerator getCmdGeneratorForBitbucket(final Map parameters = null) {

         return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null
            return getCmdForBitbucket(getCli('bitbucket', parameters), map)
        }
    }

    public CmdGenerator getCmdGeneratorForBitbucketCloud(final Map parameters = null) {

         return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null
            return getCmdForBitbucket(getCli('bitbucketcloud', parameters), map)
        }
    }

    public String getCmdForBitbucket(final String cli, final Map map) {
        def action = map?.action ?: map?.name // default action to name of task
        def fileParameter = (map?.file ? helper.jsapParameter('file', ((map?.file == true) ? gint.getOutputFile(map?.name) : map?.file)) : '')
        def project = map?.project ? helper.jsapParameter('project', map?.project) : ''
        def repository = map?.repository ? helper.jsapParameter('repository', map?.repository) : ''
        def pullRequest = map?.pullRequest ? helper.jsapParameter('pullRequest', map?.pullRequest) : ''
        def parameterString = getParameterStringForJsapGenerator(map)
        def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
        return "${cli} --action ${action} ${project} ${repository} ${pullRequest} ${fileParameter} ${parameterString} ${map?.parameters2 ?: ''} ${redirect}".toString()
    }

    /**
     * Closure that generates a command string for Slack CLI client. The closure takes a task definition
     * as a parameter. The task definition can have keys:
     * <ul>
     * <li> name - task name
     * <li> action - CLI action to take, defaults to name
     * <li> file - file CLI parameter value, if true - defaults to output file based on task name
     * <li> parameters (or parameters2) - other CLI parameters
     * <li> project - project name
     * </ul>
     * @return closure that generates cli command string for a task
     */
    public CmdGenerator getCmdGeneratorForSlack(final Map parameters = null) {

        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null
            return getCmdForSlack(getCli('slack', parameters), map)
        }
    }

    public String getCmdForSlack(final String cli, final Map map = null) {
        def action = map?.action ?: map?.name // default action to name of task
        def fileParameter = (map?.file ? helper.jsapParameter('file', ((map?.file == true) ? gint.getOutputFile(map?.name) : map?.file)) : '')
        def channel = map?.channel ? helper.jsapParameter('channel', map?.channel) : ''
        def parameterString = getParameterStringForJsapGenerator(map)
        def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
        return "${cli} --action ${action} ${channel} ${fileParameter} ${parameterString} ${map?.parameters2 ?: ''} ${redirect}".toString()
    }

    /**
     * Generic generator for a JSAP based CLI
     * @returns
     */
    public CmdGenerator getCmdGeneratorForJsap(final Map parameters = null) {

        def cli = helper.getWithExtendedLookup(parameters?.cli, 'jsapCli', 'jsap')
        return (CmdGenerator) { Task task ->
            Map map = getUserMap(task) // may be null

            def action = map?.action ?: map?.name // default action to name of task

            def fileParameter = (map?.file ? helper.jsapParameter('file', ((map?.file == true) ? gint.getOutputFile(map?.name) : map?.file)) : '')
            def parameterString = getParameterStringForJsapGenerator(map)
            def redirect = map?.redirect ?: ''  // usually "> fileName" or "> fileName 2>&1"
            return "${cli} --action ${action} ${fileParameter} ${parameterString} ${map?.parameters2 ?: ''} ${redirect}".toString()
        }
    }
}
