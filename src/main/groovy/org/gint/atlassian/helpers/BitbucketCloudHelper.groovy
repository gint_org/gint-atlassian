/*
 /*
 * Copyright (c) 2009, 2022 Bob Swift
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.helpers

import org.gint.plugin.Gint

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Gint extensions for Bitbucket Cloud
 */
@CompileStatic
@TypeChecked
class BitbucketCloudHelper extends AtlassianHelper {

    protected String projectKey  // project key

    /**
     * Constructor
     */
    BitbucketCloudHelper(final Gint gint) {
        super(gint, 'bitbucketcloud')
    }

    /**
     * Contact server and retrieve information.
     * Be as generic as possible on output scanning to handle CLI changes - ignore case and spacing.
     * <pre>
     * Expected output: "JIRA version: 4.2, build: 587, edition: Enterprise, time: 2010-10-26T16:07:09.329-0500, time zone: America/Chicago"
     * </pre>
     * @return Map with version, build, and time zone. Return null if server can not be contacted.
     */
    public Map<String, Object> getServerInfo(final inCli = null) {
        return getServerInfo(inCli instanceof Map ? inCli : [cli: inCli])
    }

    /**
     * This uses getUser as a partial replacement for not having a better getServerInfo
     */
    @Override
    public Map<String, Object> getServerInfo(final Map parameters) {
        def cli = getCli(parameters?.cli)
        Map<String, Object> info = null
        if (helper.isNotBlank(cli)) {
            def cmd = cli.trim() + ' --action getUser'  // CLI 9.7 for getUser and 10.2 for getServerInfo
            def p = gint.getParallelHelper().shell(cmd)
            def rc = p.end(false) // end with no output
            def lines = p.getOutList()
            if ((rc == 0) && (lines.size() > 0)) { // successful
                info = [:]
                info.cli = cli
                int index = 0

                def string = helper.listToSeparatedString(list: lines, separator: '\n')  // convert to string of lines for parsing

                info.product           = 'Bitbucket'
                info.hosting           = 'cloud'
                info.client            = 'bitbucketcloud'
                info.user              = helper.getPrettyFormatValue(string, 'user')
                info.userKey           = helper.getPrettyFormatValue(string, 'user key')
                info.userDisplayName   = helper.getPrettyFormatValue(string, 'user display name')
                info.userNickname      = helper.getPrettyFormatValue(string, 'nickname')
                info.email             = helper.getPrettyFormatValue(string, 'email')

                helper.removeNullValues(info)

                if (info.product == null) {
                    p.printOutList()
                    addMessage 'warning', 'Could not determine server information from the data returned from the request.'
                }

            } else if (gint.getVerbose()) {
                p.printOutList()
                p.printErrorList()
            }
        } else {
            addMessage 'warning', "Configuration missing: cli and bitbucketcloudCli properties are blank or not defined."
        }
        if (gint.verbose) {
            if (info) {
                helper.log('serverInfo', info)
                //helper.log('projectKey', getProjectKey()) // show default project key
                //helper.log('projectName', getProjectName()) // show default project key
            } else {
                helper.log('cli', cli)
            }
        }
        return info
    }

    /**
     * Return the cli that will be used
     * @return
     */
    public String getCli(final inCli = null) {
        return helper.getWithExtendedLookup(inCli ?: helper.getParameterValue('cli'), 'bitbucketcloudCli', 'acli bitbucketcloud')
    }
}
