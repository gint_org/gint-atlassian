/*
 * Copyright (c) 2009, 2022 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gint.atlassian.plugin

import org.gint.atlassian.helpers.AtlassianHelper
import org.gint.atlassian.helpers.AtlassianTaskHelper
import org.gint.atlassian.helpers.BambooHelper
import org.gint.atlassian.helpers.BitbucketCloudHelper
import org.gint.atlassian.helpers.BitbucketHelper
import org.gint.atlassian.helpers.CmdGeneratorHelper
import org.gint.atlassian.helpers.ConfluenceHelper
import org.gint.atlassian.helpers.JiraHelper
import org.gint.atlassian.helpers.JsapHelper
import org.gint.atlassian.helpers.JsmHelper
import org.gint.atlassian.helpers.ServiceDeskHelper
import org.gint.atlassian.helpers.SlackHelper
import org.gint.plugin.Gint
import org.gradle.api.Plugin
import org.gradle.api.Project

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

/**
 * Gradle plugin definition for Gint extension related to Atlassian applications
 */
@CompileStatic
@TypeChecked
class GintAtlassianPlugin implements Plugin<Project> {

    JsapHelper jsapHelper
    AtlassianHelper atlassianHelper
    BambooHelper bambooHelper
    BitbucketHelper bitbucketHelper
    BitbucketCloudHelper bitbucketCloudHelper
    ConfluenceHelper confluenceHelper
    JiraHelper jiraHelper
    JsmHelper jsmHelper
    ServiceDeskHelper serviceDeskHelper
    SlackHelper slackHelper

    @Override
    public void apply(Project project) {

        project.getPluginManager().apply(org.gint.plugin.GintPlugin.class)

        Gint gint = project.extensions.getByType(Gint)
        gint.cmdGeneratorHelper = new CmdGeneratorHelper(gint) // replace command generator with super class generator

        setHelpers(gint) // new helpers
        setGintMethods(gint)

        // Override some helpers
        gint.setTaskHelper(new AtlassianTaskHelper(gint))
    }

    /**
     * Set up customized helpers and make them available as new gint methods
     * @param gint
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected setHelpers(final Gint gint) {

        Gint.metaClass.getJsapHelper {
            if (jsapHelper == null) {
                jsapHelper = new JsapHelper(gint)
            }
            return jsapHelper
        }

        Gint.metaClass.getAtlassianHelper {
            if (atlassianHelper == null) {
                atlassianHelper = new AtlassianHelper(gint)
            }
            return atlassianHelper
        }

        Gint.metaClass.getBambooHelper {
            if (bambooHelper == null) {
                bambooHelper = new BambooHelper(gint)
            }
            return bambooHelper
        }

        Gint.metaClass.getBitbucketHelper {
            if (bitbucketHelper == null) {
                bitbucketHelper = new BitbucketHelper(gint)
            }
            return bitbucketHelper
        }

        Gint.metaClass.getBitbucketCloudHelper {
            if (bitbucketCloudHelper == null) {
                bitbucketCloudHelper = new BitbucketCloudHelper(gint)
            }
            return bitbucketCloudHelper
        }

        Gint.metaClass.getConfluenceHelper {
            if (confluenceHelper == null) {
                confluenceHelper = new ConfluenceHelper(gint)
            }
            return confluenceHelper
        }

        Gint.metaClass.getJiraHelper {
            if (jiraHelper == null) {
                jiraHelper = new JiraHelper(gint)
            }
            return jiraHelper
        }

        Gint.metaClass.getJsmHelper {
            if (jsmHelper == null) {
                jsmHelper = new JsmHelper(gint)
            }
            return jsmHelper
        }

        Gint.metaClass.getServiceDeskHelper {  // deprecated
            if (serviceDeskHelper == null) {
                serviceDeskHelper = new ServiceDeskHelper(gint)
            }
            return serviceDeskHelper
        }

        Gint.metaClass.getSlackHelper {
            if (slackHelper == null) {
                slackHelper = new SlackHelper(gint)
            }
            return slackHelper
        }

        //        Gint.metaClass.getSeleniumHelper {
        //
        //            println 'get selenium helper: ' + gint.@seleniumHelper
        //            //println 'get jira helper:     ' + gint.@jiraHelper
        //
        //            if (gint.@seleniumHelper == null) {
        //                if (this.@jiraHelper != null) {
        //                    seleniumHelper = new SeleniumHelperForJira(gint)
        //                } else if (this.@confluenceHelper != null) {
        //                    seleniumHelper = new SeleniumHelperForConfluence(gint)
        //                } else {
        //                    seleniumHelper = new SeleniumHelperForAtlassian(gint)
        //                }
        //            }
        //            return seleniumHelper
        //        }
        //
        //        gint.getJiraHelper()
        //        gint.getSeleniumHelper()
        //        println 'get selenium helper 2: ' + gint.getSeleniumHelper()
        //        println 'get jira helper 2:     ' + gint.getJiraHelper()
    }

    /**
     * Set up customized helpers and make them available as new gint methods
     * @param gint
     * @return
     */
    @CompileStatic(TypeCheckingMode.SKIP)
    protected setGintMethods(final Gint gint) {

        Gint.metaClass.searchForId { Object data, int occurrence = 1 ->
            return gint.getJsapHelper().searchForId(data, occurrence)
        }

        Gint.metaClass.searchForUrl { Object data, int occurrence = 1 ->
            return gint.getJsapHelper().searchForUrl(data, occurrence)
        }

        Gint.metaClass.searchForIssueKey { Object data, int occurrence = 1 ->
            return gint.getJiraHelper().searchForIssueKey(data, occurrence)
        }
    }
}
