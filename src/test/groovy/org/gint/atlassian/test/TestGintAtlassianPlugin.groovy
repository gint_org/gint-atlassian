/*
 * Copyright (c) 2009, 2018 Bob Swift
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Bootstrap testing - gradlew test --rerun-tasks

package org.gint.atlassian.test

import static org.junit.Assert.*

import org.gint.atlassian.plugin.Constants
import org.gint.plugin.Gint
import org.gint.tasks.GintInternalTask
import org.gradle.api.Project
import org.gradle.internal.impldep.com.google.api.client.googleapis.testing.TestUtils
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

class TestGintAtlassianPlugin {

    private void createBuildFile(final String content = '') throws IOException {
        File buildFile = testProjectDir.newFile("build.gradle")
        TestUtils.writeFile(buildFile, "plugins { id '${Constants.PLUGIN_ID}' }" + '\n' + content, content != '')
    }

    @Rule
    public final TemporaryFolder testProjectDir = new TemporaryFolder() // creates/deletes before/after run

    @Test
    public void testDefinedTasks() {

        Project project = ProjectBuilder.builder().build()

        project.pluginManager.apply Constants.PLUGIN_ID

        Gint gint = project.extensions.getByType(Gint)
        assertTrue(gint != null)

        assertTrue(project.tasks[org.gint.plugin.Constants.TASK_ALL] instanceof GintInternalTask) // Verify gint plugin is automatically loaded by this plugin
        assertTrue(gint.getSlackHelper() != null)
    }
}
